<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyComment extends Model
{
    protected $table = 'property_comment';
}
