<?php
/**
 * UserController File Document Comment
 *
 * PHP Version 7.1
 *
 * @category Controllers
 * @package  Controller
 * @author   Mobiloitte <it@mobiloitte.in>
 * @license  http://mobiloitte.com/ GNU General Public License
 * @link     app/Http/Controllers/Admin
 */
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\paginate;
use App\Http\Controllers\Controller;
use App\Rules\EmailExists;
use Storage;
use App\UserRole;
use App\User;
use App\Property;
use Auth;
use Redirect;

/**
 * UserController Class Doc Comment
 *
 * @category Class
 * @package  Controller
 * @author   Mobiloitte <it@mobiloitte.in>
 * @license  http://mobiloitte.com/ GNU General Public License
 * @link     app/Http/Controllers/Admin
 */

class UserController extends Controller
{

	/**
    *Function Name : index
    *Description : After login redirect to dashboard
    *
    * @param email            User ID
    * @param password      	  User password 
    *
    * @return response 
    */	
   public function index(Request $request)
   {
    try 
    {
  		$email = $request->input('email');
  		$password = $request->input('password');
  		$attempt = auth()->attempt(['email' => $email, 'password' => $password]);

		if(! $attempt)
		{
			Auth::logout();
			return Redirect::route('admin.login');
		}
		else
			return Redirect::route('admin.dashboard');
      } catch (Exception $e) {
        return Response::json(["line" => $e->getLine(), "msg" => $e->getMessage()], 500); 
      }			
   }
   /**
    *Function Name : dashboard
    *Description : Display all counts on dashboard
    *
    *
    * @return response 
    */  
   public function dashboard(Request $request)
   {

    try {
        // Count of users    
        $userCount = User::where('is_deleted', 0)->where('id', '!=', '1')->count(); 
        // Count of property
        $propertyCount = Property::where('is_deleted', 0)->count(); 
        // Count of upload documents
        $addressCount =User::where('is_deleted', 0)->where('id', '!=', '1')->where('address_verified', '!=', '2')->where('documents', '!=', '')->count(); 

        return view('admin.dashboard', ['usercount' => $userCount, 'propertyCount' => $propertyCount, 'addresscount' => $addressCount]);
    } catch (Exception $e) {
      return Response::json(["line" => $e->getLine(), "msg" => $e->getMessage()], 500); 
    }

      
   }

   /**
    *Function Name : userList
    *Description : Display all user List
    *
    *
    * @return response 
    */	
   public function userList(Request $request)
   {

    try {
        // All user list with pagination
        $userList = User::where('is_deleted', 0)->where('id', '!=', '1')->orderBy('id','DESC')->paginate(10); 

        return view('admin.user_list', ['users' => $userList]);

    } catch (Exception $e) {

      return Response::json(["line" => $e->getLine(), "msg" => $e->getMessage()], 500); 
    }
   }

   /**
    *Function Name : userView
    *Description : Display user Details
    *
    * @param user_id         User's Id
    *
    * @return response 
    */  
   public function userView(Request $request)
   {

    try {

      // User Details
      $userView = User::where('id', $request->id)->first(); 
      return view('admin.view_profile', ['user' => $userView]);
    } catch (Exception $e) {
      return Response::json(["line" => $e->getLine(), "msg" => $e->getMessage()], 500); 
    }

   }

     /**
    *Function Name : userView
    *Description : Display user Details
    *
    *
    * @return response 
    */  
   public function documentUserList(Request $request)
   {

    try {
      
      //User's Document list 
      $userList = User::where('is_deleted', 0)->where('id', '!=', '1')->where('address_verified', '!=', '2')->where('documents', '!=', '')->paginate(10); 
      return view('admin.document_user_list', ['users' => $userList]);
    } catch (Exception $e) {
      return Response::json(["line" => $e->getLine(), "msg" => $e->getMessage()], 500); 
    }

   }

   /**
    *Function Name : documentView
    *Description : Display user Documentes
    *
    * @param user_id         User's Id 
    *
    * @return response 
    */  
   public function documentView(Request $request)
   {
    try {

      // user's documents
      $userView = User::where('id', $request->id)->first(); 
      return view('admin.view_document', ['user' => $userView]);
    } catch (Exception $e) {
      return Response::json(["line" => $e->getLine(), "msg" => $e->getMessage()], 500); 
    }
   }

   /**
    *Function Name : documentApprove
    *Description : Approved the documents
    *
    * @param user_id            User's ID
    *
    * @return response 
    */  
   public function documentApprove(Request $request)
   {

    try {
        // Find user's Details
        $userDetails = User::where('id', $request->id)->first(); 
        $userDetails->address_verified = 2;
        $userDetails->save();
        session()->flash('status', 'Documents approved sucessfully');
        session()->flash('alert-class', 'alert-info');
        return redirect()->back();
    } catch (Exception $e) {
      return Response::json(["line" => $e->getLine(), "msg" => $e->getMessage()], 500);
    }    
   }
   /**
    *Function Name : userBlocked
    *Description : User blocked and unblocked
    *
    * @param id            User's ID
    * @param status        User's status 
    *
    * @return response 
    */  
   public function userBlocked(Request $request)
   {
    try {
        $id     = $request->get('id');
        $status = $request->get('status') ;
        $user   = User::find($id);
        $user->is_blocked = $status;
        $user->save();

    } catch (Exception $e) {
      return Response::json(["line" => $e->getLine(), "msg" => $e->getMessage()], 500);
    }
   }

   /**
    *Function Name : userDelete
    *Description : User deleted 
    *
    * @param id            User's ID 
    *
    * @return response 
    */  
   public function userDelete(Request $request)
   {
    try {
        $id     = $request->get('id');
        $user   = User::find($id);
        $user->is_deleted = 1;
        $user->save();

    } catch (Exception $e) {
      return Response::json(["line" => $e->getLine(), "msg" => $e->getMessage()], 500);
    }
   }

   /**
    *Function Name : adminView
    *Description : Display Admin Details
    *
    * @param user_id         User's Id
    *
    * @return response 
    */  
   public function adminView(Request $request)
   {

    try {

      // User Details
      $userView = User::where('id', '1')->first(); 
      return view('admin.admin_profile', ['user' => $userView]);
    } catch (Exception $e) {
      return Response::json(["line" => $e->getLine(), "msg" => $e->getMessage()], 500); 
    }

   }
   /**
    *Function Name : adminProfileEdit
    *Description : Edit Admin Profile
    *
    * @param user_id         User's Id
    *
    * @return response 
    */  
   public function adminProfileEdit(Request $request)
   {

    try {

      // User Details
      $userView = User::where('id', '1')->first(); 
      return view('admin.edit_admin_profile', ['user' => $userView]);
    } catch (Exception $e) {
      return Response::json(["line" => $e->getLine(), "msg" => $e->getMessage()], 500); 
    }

   }

   /**
     * Update user's profile.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    */

     public function adminUpdateProfile(Request $request)
     {
        
            $validatedData = $request->validate([
                'email' => 'required|email|',new EmailExists,
                'full_name' => 'required|min:2|max:90',
                'file' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
        try{    
            $user = User::find(auth()->user()->id);
            $user->email =  $request->email;
            $user->name =  $request->full_name;            
            
            //to upload photo 
            if($request->hasFile('file')) {
                $image = $request->file('file');
                $image =  Storage::putFileAs('uploads/profile_images', $image, $image->getClientOriginalName());
                $user->profile_image = $image;
            }

            $user->save();
            
            return redirect('admin/user/view')->with('status', 'Profile updated sucessfully.');
        } catch (Exception $e) {
            return Response::json(["line" => $e->getLine(), "msg" => $e->getMessage()], 500);
        }
    }
}
