<?php
/**
 * PasswordController File Document Comment
 *
 * PHP Version 7.1
 *
 * @category Controllers
 * @package  Controller
 * @author   Mobiloitte <it@mobiloitte.in>
 * @license  http://mobiloitte.com/ GNU General Public License
 * @link     app/Http/Controllers/Website
*/
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Illuminate\Routing\Redirector;
use DB;
use Carbon\Carbon;
use Auth;
use Hash;

/**
 * PasswordController Class Doc Comment
 *
 * @category Class
 * @package  Controller
 * @author   Mobiloitte <it@mobiloitte.in>
 * @license  http://mobiloitte.com/ GNU General Public License
 * @link     app/Http/Controllers/Website
*/

class PasswordController extends Controller
{
    /**
     * Function Name : sendPasswordLink
     * Description   : Send password link to user for genrating new password
     *
     * @return response
    */
    public function sendPasswordLink(Request $request)
    {
        try{    
            $user = User::where('email', $request->email)->first();
            if ( !$user ) return redirect()->back()->withErrors(['email' => 'Email ID does not exists.']);

            //create a new token to be sent to the user. 
            DB::table('password_resets')->insert([
                'email' => $request->email,
                'token' => str_random(60), //change 60 to any length you want
                'created_at' => Carbon::now()
            ]);

            $tokenData = DB::table('password_resets')
            ->where('email', $request->email)->first();

            $token = $tokenData->token;
            $email = $request->email; 

            $data = ['url'=>url('reset-password/' . $token),'email'=>$request->email,'token'=>$token];
           // print_r($data);exit;

            Mail::send('mail_template.reset_mail', $data, function($message) use ($data) {
             $message->to($data['email'])->subject('Reset Password Link');
            });
            session()->flash('message','Reset link sent successfully to registered email id.');
            session()->flash('alert-class','alert-info');
            return redirect()->route('forgot-password');
        } catch (Exception $e) {
            return Response::json(["line" => $e->getLine(), "msg" => $e->getMessage()], 500);
        }
    }

    /**
     * Function Name : showPasswordResetForm
     * Description   : show password reset form
     *
     * @return response
    */

    public function showPasswordResetForm($token)
    {
        try{
            $tokenData = DB::table('password_resets')->where('token', $token)->first();

            if ( !$tokenData ) return redirect()->route('login'); //redirect them anywhere you want if the token does not exist.
              return view('website.passwords',compact('token'));
         } catch (Exception $e) {
            return Response::json(["line" => $e->getLine(), "msg" => $e->getMessage()], 500);
        }
    }
    
    /**
     * reset password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    */
     public function resetPassword(Request $request, $token)
     {

        try{
            $user = User::where('email', $request->email)->first();
            if ( !$user ) return redirect()->back()->withInput()->withErrors(['email' => 'Email address does not exists.']);
                $password = $request->password;
                $tokenData = DB::table('password_resets')->where('token', $token)->first();

                $user = User::where('email', $tokenData->email)->first();

            if ( !$user ) return redirect()->to('login'); //or wherever you want

                $user->password = Hash::make($password);
                $user->update(); //or $user->save();

             //do we log the user directly or let them login and try their password for the first time ? if yes 
                Auth::login($user);

            // If the user shouldn't reuse the token later, delete the token 
                DB::table('password_resets')->where('email', $user->email)->delete();
                return redirect()->url('/');
        } catch (Exception $e) {
            return Response::json(["line" => $e->getLine(), "msg" => $e->getMessage()], 500);
        }
      
     }


    /**
     * Validate email address.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    */
    public function checkEmail(Request $request)
    {
        try{
            $check = User::where('email',$request->email)->first();
        
            if($check)    
                return 0;
            else
                return 1;
        } catch (Exception $e) {
            return Response::json(["line" => $e->getLine(), "msg" => $e->getMessage()], 500);
        }
    }
}    