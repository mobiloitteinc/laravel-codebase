<?php

/**
 * Dashboard File Document Comment
 *
 * PHP Version 7.0
 *
 * @category Controllers
 * @package  Controller
 * @author   Akaash <akaash.mehra@mobiloitte.in>
 * @license  http://mobiloitte.com/ GNU General Public License
 * @link     app/Http/Controllers/Api
 */

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\UserDevice;
use App\User;
use App\UserPasswordVerification;
use App\UserNeighbour;
use App\Property;
use App\Tutorial;
use App\FollowFollowing;
use App\AppReview;
use App\StaticContent;
use App\PropertyImage;
use App\PropertyAttribute;
use App\PropertyFloorPlan;
use App\Attribute;
use App\PropertyFollow;
use App\PropertyLike;
use App\PropertyComment;
use App\Mail\ForgotPassword;
use Auth;
use Response;
use Mail;
use Hash;
use Exception;
use Carbon\Carbon;
use DB;

class PropertyController extends Controller
{
	/**
	*Function Name : addProperty
	*Description : Add the property
	*
	* @param user_id 			User ID
	* @param property_name 		Property Name
	* @param property_address   Address of Property
	* @param property_type  	Type of property, 1 for apartment, 2 for house, 3 for room 
	* @param property_for 		Property For, 1 for sell, 2 for rent 
	* @param zip_code 		    Zip code of property
	* @param latitude 			Latitude of property 
	* @param longitude 		    Longitude of property
	* @param no_of_bedrooms 	Number of bedrooms 
	* @param no_of_bathrooms 	Number of bathrroms
	* @param parking_garage     Parking Garage 
	* @param preffered_suburb   Preferd Subrb 
	* @param reason_for_selling Reason for selling  
	* @param description        Description 
	* @param price_for_property Price for property 
	* @param is_pet_allowed     Pet for 0 for not allow, 1 for allowed
	* @param image     			Property Images
	*
	* @return response 
	*/
   public function addProperty(Request $request)
   {

   	try {

   		// Check if parameter exists
   		if(empty($request->user_id) || empty($request->property_for) || empty($request->property_name) || empty($request->property_address)|| empty($request->zip_code)|| empty($request->latitude)|| empty($request->longitude)|| empty($request->property_type)|| empty($request->no_of_bedrooms)|| empty($request->no_of_bathrooms)|| empty($request->other_living_room) || empty($request->parking_garage)  || empty($request->preffered_suburb) || empty($request->reason_for_selling) || empty($request->description) || empty($request->price_for_property)  || empty($request->image) || empty($request->main_image) ) {
   			 return \Response::json(['message' => 'All fields are required.', 'response_code' => 400], 400);
   		}

   		//Check User Delete
        if($this->userDeleted($request->get('user_id'))){
            return \Response::json(['message'=>'Your account has been deleted. Please contact to admin.', 'response_code' => 302], 302);
        }

        //Check User Block
        if($this->userBlocked($request->get('user_id'))){
            return \Response::json(['message'=>'You are blocked by admin. Please contact to admin.', 'response_code' => 303], 303);
        }


        // Save property details
        $addProperty 						= new Property;
        $addProperty->user_id 				= $request->user_id;
        $addProperty->property_for 			= $request->property_for;
        $addProperty->property_name 		= $request->property_name;
        $addProperty->property_address 		= $request->property_address;
        $addProperty->zip_code 				= $request->zip_code;
        $addProperty->latitude 				= $request->latitude;
        $addProperty->longitude 			= $request->longitude;
        $addProperty->property_type 		= $request->property_type;
        $addProperty->no_of_bedrooms 		= $request->no_of_bedrooms;
        $addProperty->no_of_bathrooms 		= $request->no_of_bathrooms;
        $addProperty->other_living_room 	= $request->other_living_room;
        $addProperty->parking_garage 		= $request->parking_garage;
        $addProperty->preffered_suburb 		= $request->preffered_suburb;
        $addProperty->reason_for_selling 	= $request->reason_for_selling;
        $addProperty->description 			= $request->description;
        $addProperty->price_for_property 	= $request->price_for_property;
        $addProperty->is_pet_allowed        = $request->is_pet_allowed;
        //$addProperty->main_image 		    = $request->main_image;
        $addProperty->price_type 		    = $request->price_type;
         // Profile Image Check
        if (!empty($request->main_image)) {

            $image = $request->main_image;
            $imageMainProperty =  Storage::putFileAs('uploads/property_image', $image, $image->getClientOriginalName());
        }
        $addProperty->main_image 		    = $imageMainProperty;
        $addProperty->save();

     
        // Upload Images of Property
        foreach ($request->image as $key => $value) {
        
        	if (!empty($value)) {
               
                $imageProperty =  Storage::putFileAs('uploads/property_image', $value, $value->getClientOriginalName());
            }
            //Save property image
            $addImage  					= new PropertyImage;
            $addImage->images           = $imageProperty;
            $addImage->property_id      = $addProperty->id;
            $addImage->save();
        }

        //property floor plan
        if($request->floor_plan_image){
        	$floorImg = $request->floor_plan_image;
        	// Save Image
        	$PropertyFloorPlanImage  				=  Storage::putFileAs('uploads/property_image', $floorImg, $floorImg->getClientOriginalName());

        	$addPropertyFloorPlan 					= new PropertyFloorPlan;
        	$addPropertyFloorPlan->property_id 		= $addProperty->id;
        	$addPropertyFloorPlan->floor_plan_image = $PropertyFloorPlanImage;
        	$addPropertyFloorPlan->save();
        }

        // Add attribute of Property
        foreach ($request->attribute_id as $key => $aid) {
        
           
            $addAttribute                   = new PropertyAttribute;
            $addAttribute->attribute_id     = $aid;
            $addAttribute->property_id      = $addProperty->id;
            $addAttribute->save();
        }


        // Details of property
        $property = Property::where('id', $addProperty->id)->first();

        $resp['id']                    	= $property->id;

        //Property For, 1 for sell, 2 for rent 
        if($property->property_for == 1){
        	$resp['property_for']          	= "Sell";
        }else{
        	$resp['property_for']          	= "Rent";
        }

        //Type of property, 1 for apartment, 2 for house, 3 for room 
        if($property->property_type == 1){
        	$resp['property_type'] 			= 'Apartment';
        }elseif($property->property_type == 2){
        	$resp['property_type'] 			= 'House';
        }else{
        	$resp['property_type'] 			= 'Room';
        }

        $resp['property_name']         	= $property->property_name;
        $resp['property_address']      	= $property->property_address;
        $resp['zip_code']              	= $property->zip_code;
        $resp['latitude'] 				= $property->latitude;
        $resp['longitude'] 				= $property->longitude;
        $resp['no_of_bedrooms'] 		= $property->no_of_bedrooms;
        $resp['no_of_bathrooms'] 		= $property->no_of_bathrooms;
        $resp['other_living_room'] 		= $property->other_living_room;
        $resp['parking_garage'] 		= $property->parking_garage;
        $resp['preffered_suburb'] 		= $property->preffered_suburb;
        $resp['reason_for_selling'] 	= $property->reason_for_selling;
        $resp['description'] 			= $property->description;
        $resp['price_for_property'] 	= $property->price_for_property;
        $resp['is_pet_allowed'] 		= $property->is_pet_allowed;

		// Get all Images of properties 
		$allImg = array();        
        $propertyImages = PropertyImage::where('property_id', $addProperty->id)->get();
        foreach ($propertyImages as $key => $value) {
         	  $img['property_image']  = ($value->images) ? url('storage/'.$value->images) : "";
         	  $allImg[] = $img;
        }
        $resp['allImages'] = $allImg; 

        // Get all attribute of properties        
        $propertyAttributes = PropertyAttribute::with('attributeName')->where('property_id', $addProperty->id)->get();
        $allAttribute = array();
        foreach ($propertyAttributes as $key => $val) {
         
         	 $attr['id']             	= $val->attributeName->id;
         	 $attr['attribute_name'] 	= $val->attributeName->attribute_name;
         	 $allAttribute[] 			= $attr;
        }
        $resp['attributeDetails']  = $allAttribute;

      


        return \Response::json(['data' => $resp, 'message'=>'Property added successfully.', 'response_code' => 200], 200);
   		
   	} catch (Exception $e) {
   		return Response::json(["line" => $e->getLine(), "message" => $e->getMessage()], 500);
   	}

   }

   /**
	*Function Name : propertyDetails
	*Description : Property Details
	*
	* @param user_id 			User ID
	* @param property_id 		Property ID
	*
	* @return response 
	*/
   public function propertyDetails(Request $request)
   {

   	try {

   		// Check if parameter exists
   		if( empty($request->user_id) || empty($request->property_id) ) {
   			 return \Response::json(['message' => 'All fields are required.', 'response_code' => 400], 400);
   		}

   		//Check User Delete
        if($this->userDeleted($request->get('user_id'))){
            return \Response::json(['message'=>'Your account has been deleted. Please contact to admin.', 'response_code' => 302], 302);
        }

        //Check User Block
        if($this->userBlocked($request->get('user_id'))){
            return \Response::json(['message'=>'You are blocked by admin. Please contact to admin.', 'response_code' => 303], 303);
        }


        // Details of property
        $property = Property::where('id', $request->property_id)->first();

        // Details of property floor plan
        $floorPlanImage = PropertyFloorPlan::where('property_id', $request->property_id)->first();

        // User Details of property
        $userDetails = User::where('id', $property->user_id)->first();

        $user['user_id']        = $userDetails->id;
        $user['full_name']      = $userDetails->name;
        $user['phone_number']   = $userDetails->phone_number;
        $user['country_code']   = $userDetails->country_code;
        $user['gender']         = $userDetails->gender;
        $user['email']          = $userDetails->email;
        $user['date_of_birth']  = ($userDetails->date_of_birth) ? $userDetails->date_of_birth : "";
        $user['cover_image']    = ($userDetails->cover_image) ? url('storage/'.$userDetails->cover_image) : "";
        $user['profile_image']  = ($userDetails->profile_image) ? url('storage/'.$userDetails->profile_image) : "";
        $user['apartment']      = ($userDetails->apartment) ? $userDetails->apartment : "";

        $resp['user_details']   = $user;
        $resp['id']             = $property->id;

        //Property For, 1 for sell, 2 for rent 
        if($property->property_for == 1){
        	$resp['property_for']          	= "Sell";
        }else{
        	$resp['property_for']          	= "Rent";
        }

        //Type of property, 1 for apartment, 2 for house, 3 for room 
        if($property->property_type == 1){
        	$resp['property_type'] 			= 'Apartment';
        }elseif($property->property_type == 2){
        	$resp['property_type'] 			= 'House';
        }else{
        	$resp['property_type'] 			= 'Room';
        }

        $resp['property_name']         	= $property->property_name;
        $resp['property_address']      	= $property->property_address;
        $resp['zip_code']              	= $property->zip_code;
        $resp['latitude'] 				= $property->latitude;
        $resp['longitude'] 				= $property->longitude;
        $resp['no_of_bedrooms'] 		= $property->no_of_bedrooms;
        $resp['no_of_bathrooms'] 		= $property->no_of_bathrooms;
        $resp['other_living_room'] 		= $property->other_living_room;
        $resp['parking_garage'] 		= $property->parking_garage;
        $resp['preffered_suburb'] 		= $property->preffered_suburb;
        $resp['reason_for_selling'] 	= $property->reason_for_selling;
        $resp['description'] 			= $property->description;
        $resp['price_for_property'] 	= $property->price_for_property;
        $resp['is_pet_allowed'] 		= $property->is_pet_allowed;
        $resp['main_image'] 		    = $property->main_image ? url('storage/'.$property->main_image) : '';
        $resp['floor_plan_image'] 		= optional($floorPlanImage)->floor_plan_image ? url('storage/'.$property->floor_plan_image) : '';

		// Get all Images of properties 
		$allImg = array();        
        $propertyImages = PropertyImage::where('property_id', $request->property_id)->get();
        foreach ($propertyImages as $key => $value) {
         	  $img['property_image']  = ($value->images) ? url('storage/'.$value->images) : "";
         	  $allImg[] = $img;
        }
        $resp['allImages'] = $allImg; 

        // Get all attribute of properties        
        $propertyAttributes = PropertyAttribute::with('attributeName')->where('property_id', $request->property_id)->get();
        $allAttribute = array();
        foreach ($propertyAttributes as $key => $val) {
         
         	 $attr['id']             	= $val->attributeName->id;
         	 $attr['attribute_name'] 	= $val->attributeName->attribute_name;
         	 $allAttribute[] 			= $attr;
        }
        $resp['attributeDetails']  = $allAttribute;
        
        return \Response::json(['data' => $resp, 'message'=>'Product details fetch successfully.', 'response_code' => 200], 200);
   		
   	} catch (Exception $e) {
   		return Response::json(["line" => $e->getLine(), "message" => $e->getMessage()], 500);
   	}

   }

   /**
    *Function Name : propertyList
    *Description : Property List according to Property Type and property for
    *
    * @param user_id            User ID
    * @param property_type      Type of property, 1 for apartment, 2 for house, 3 for room 
    * @param property_for       Property For, 1 for sell, 2 for rent 
    * @param zip_code           Zip Code of User 
    *
    * @return response 
    */
   public function propertyList(Request $request)
   {

    try {

		$page_number = $request->page_number;
		$page_size   = $request->page_size;
		$distance = $request->distance;

		// Check if parameter exists
        if( empty($request->user_id) || empty($request->zip_code)  || empty($page_number) || empty($page_size) ) {
             return \Response::json(['message' => 'All fields are required.', 'response_code' => 400], 400);
        }

        //Check User Delete
        if($this->userDeleted($request->get('user_id'))){
            return \Response::json(['message'=>'Your account has been deleted. Please contact to admin.', 'response_code' => 302], 302);
        }

        //Check User Block
        if($this->userBlocked($request->get('user_id'))){
            return \Response::json(['message'=>'You are blocked by admin. Please contact to admin.', 'response_code' => 303], 303);
        }
        
        $propertyAll = array();
        $allProperty = array();
        // check if latitude and longitude and distance
		if($distance && $request->lat && $request->long){
			$propertyCount = Property::selectRaw("*, ( ( 6371 * acos( cos( radians($request->lat) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians($request->long) ) + sin( radians($request->lat) ) * sin( radians( latitude ) ) ) ) ) AS distance");

			$propertyCount->having('distance', '<=', $distance);

			//$allProperty = $propertyCount->get();
			if($request->property_for){
        		$propertyCount->where('property_for', $request->property_for);
        	}
	        // Filter Property For 
	        if($request->property_type){
	        	$propertyCount->where('property_type', $request->property_type);
	        }
	        $propertyCount->where('user_id' ,'!=', $request->user_id);
	        $propertyCount->where('is_deleted' , 0);
	        $propertyCount->where('is_blocked' , 0);
	        // Property accending or decending order
	        if($request->order_by){
	        	$propertyCount->orderBy('id',$request->order_by);
	        }
	        //Price Filter 
	        if(count($request->min_price) && $request->max_price){
	          // This will only executed if you received any price
	           $propertyCount->where('price_for_property','>=',$request->min_price);
	           $propertyCount->where('price_for_property','<=',$request->max_price);
	      	}
	      	// Property sorting by price
	        if($request->sort_by_price){
	        	$propertyCount->orderBy('price_for_property',$request->sort_by_price);
	        }
	      	// Property search by name and descrition
	        if($request->search){
	        	$propertyCount->where('property_name','like', '%' .$request->search. '%');
	        	$propertyCount->orWhere('description','like', '%' .$request->search. '%');
	        }
	         $propertyCountAll = $propertyCount->get();

        }else{

        	// Get All Property List according to zip code
	        $propertyCount = Property::where('zip_code', $request->zip_code);
	        if($request->property_for){
	        	$propertyCount->where('property_for', $request->property_for);
	    	}
	        if($request->property_type){
	        	$propertyCount->where('property_type', $request->property_type);
	        }
	        $propertyCount->where('user_id' ,'!=', $request->user_id);
	        $propertyCount->where('is_deleted' , 0);
	        $propertyCount->where('is_blocked' , 0);
	        // Property sorting accending or decending order
	        if($request->sort_by_asc_desc){
	        	$propertyCount->orderBy('id',$request->sort_by_asc_desc);
	        }
	        //Price Filter 
	        if(count($request->min_price) && $request->max_price){
	          // This will only executed if you received any price
	           $propertyCount->where('price_for_property','>=',$request->min_price);
	           $propertyCount->where('price_for_property','<=',$request->max_price);
	      	}
	      	// Property sorting by price
	        if($request->sort_by_price){
	        	$propertyCount->orderBy('price_for_property',$request->sort_by_price);
	        }
	      	// Property search by name and descrition
	        if($request->search){
	        	$propertyCount->where('property_name','like', '%' .$request->search. '%');
	        	$propertyCount->orWhere('description','like', '%' .$request->search. '%');
	        }
	        $propertyCountAll = $propertyCount->get();
    	}

		/* Pagination Code Starts Here */
		$countData      = count($propertyCountAll);
		$start_index    = ($page_number - 1) * $page_size;
		$Pages          = ($countData / $page_size);
		$maxPages       = ceil($Pages);

		$pagination['page_number']      = $page_number;
		$pagination['totalRecords']     = $countData;
		$pagination['maxPages']         = $maxPages;
		/* Pagination Code Ends Here */

		// check if latitude and longitude and distance
		if($distance && $request->lat && $request->long){
			$property = Property::selectRaw("*, ( ( 6371 * acos( cos( radians($request->lat) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians($request->long) ) + sin( radians($request->lat) ) * sin( radians( latitude ) ) ) ) ) AS distance");

			$property->having('distance', '<=', $distance);
			//$allProperty = $property->get();
			if($request->property_for){
        		$property->where('property_for', $request->property_for);
        	}
	        // Filter Property For 
	        if($request->property_type){
	        	$property->where('property_type', $request->property_type);
	        }
	        $property->where('user_id' ,'!=', $request->user_id);
	        $property->where('is_deleted' , 0);
	        $property->where('is_blocked' , 0);
	        // Property accending or decending order
	        if($request->order_by){
	        	$property->orderBy('id',$request->order_by);
	        }
	        //Price Filter 
	        if(count($request->min_price) && $request->max_price){
	          // This will only executed if you received any price
	           $property->where('price_for_property','>=',$request->min_price);
	           $property->where('price_for_property','<=',$request->max_price);
	      	}
	      	// Property sorting by price
	        if($request->sort_by_price){
	        	$property->orderBy('price_for_property',$request->sort_by_price);
	        }
	      	// Property search by name and descrition
	        if($request->search){
	        	$property->where('property_name','like', '%' .$request->search. '%');
	        	$property->orWhere('description','like', '%' .$request->search. '%');
	        }
	        $propertyAll = $property->offset($start_index)->limit($page_size)->get();

        }else{
        
			$property = Property::where('zip_code', $request->zip_code);
	       	if($request->property_for){
        		$property->where('property_for', $request->property_for);
        	}
	        // Filter Property For 
	        if($request->property_type){
	        	$property->where('property_type', $request->property_type);
	        }
	        $property->where('user_id' ,'!=', $request->user_id);
	        $property->where('is_deleted' , 0);
	        $property->where('is_blocked' , 0);
	        // Property accending or decending order
	        if($request->order_by){
	        	$property->orderBy('id',$request->order_by);
	        }
	        //Price Filter 
	        if(count($request->min_price) && $request->max_price){
	          // This will only executed if you received any price
	           $property->where('price_for_property','>=',$request->min_price);
	           $property->where('price_for_property','<=',$request->max_price);
	      	}
	      	// Property sorting by price
	        if($request->sort_by_price){
	        	$property->orderBy('price_for_property',$request->sort_by_price);
	        }
	      	// Property search by name and descrition
	        if($request->search){
	        	$property->where('property_name','like', '%' .$request->search. '%');
	        	$property->orWhere('description','like', '%' .$request->search. '%');
	        }
	        $propertyAll = $property->offset($start_index)->limit($page_size)->get();
    	}

		

        foreach ($propertyAll as $key => $value) {

            // Get all Images of properties        
            $propertyImages   = PropertyImage::where('property_id', $value->id)->get();
            // Find User follow to property
            $propertyFollower = PropertyFollow::where('user_id', $request->user_id)->where('property_id', $value->id)->first();
            // Find User like to property
            $propertyLikes    = propertyLike::where('user_id', $request->user_id)->where('property_id', $value->id)->first();
            $allImg = array();
            foreach ($propertyImages as $key => $values) {
                  $img['property_image']  = ($values->images) ? url('storage/'.$values->images) : "";
                  $allImg[] = $img;
             }

            // Get all attribute of properties        
	        $propertyAttributes = PropertyAttribute::with('attributeName')->where('property_id', $value->id)->get();
	        $allAttribute = array();
	        foreach ($propertyAttributes as $key => $val) {
	         
	         	 $attr['id']             	= $val->attributeName->id;
	         	 $attr['attribute_name'] 	= $val->attributeName->attribute_name;
	         	 $allAttribute[] 			= $attr;
	        }

	        // Details of property floor plan
        	$floorPlanImage = PropertyFloorPlan::where('property_id', $value->id)->first();

	        // User Details of property
	        $userDetails = User::where('id', $value->user_id)->first();

            $user['user_id']        = $userDetails->id;
            $user['full_name']      = $userDetails->name;
            $user['phone_number']   = $userDetails->phone_number;
            $user['country_code']   = $userDetails->country_code;
            $user['gender']         = $userDetails->gender;
            $user['email']          = $userDetails->email;
            $user['date_of_birth']  = ($userDetails->date_of_birth) ? $userDetails->date_of_birth : "";
            $user['cover_image']    = ($userDetails->cover_image) ? url('storage/'.$userDetails->cover_image) : "";
            $user['profile_image']  = ($userDetails->profile_image) ? url('storage/'.$userDetails->profile_image) : "";
            $user['apartment']      = ($userDetails->apartment) ? $userDetails->apartment : "";
            $resp['user_details']   = $user;

            // Type of property, 1 for apartment, 2 for house, 3 for room 
            if($value->property_type == 1){
                $resp['property_type']          = 'Apartment';
            }else if($value->property_type == 2){
                $resp['property_type']          = 'House';
            }else{
                $resp['property_type']          = 'Room';
            }

            // Check user follow or not
            if($propertyFollower){
            	$resp['following']         = true;
            }else{
            	$resp['following']         = false;
            }

            // Check user like or not
            if($propertyLikes){
            	$resp['like']         = true;
            }else{
            	$resp['like']         = false;
            }

            $resp['id']                     = $value->id;
            $resp['property_name']          = $value->property_name;
            $resp['property_address']       = $value->property_address;
            $resp['zip_code']               = $value->zip_code;
            $resp['latitude']               = $value->latitude;
            $resp['longitude']              = $value->longitude;
            $resp['no_of_bedrooms']         = $value->no_of_bedrooms;
            $resp['no_of_bathrooms']        = $value->no_of_bathrooms;
            $resp['other_living_room']      = $value->other_living_room;
            $resp['parking_garage']         = $value->parking_garage;
            $resp['preffered_suburb']       = $value->preffered_suburb;
            $resp['reason_for_selling']     = $value->reason_for_selling;
            $resp['description']            = $value->description;
            $resp['price_for_property']     = $value->price_for_property;
            $resp['is_pet_allowed']         = $value->is_pet_allowed;
            $resp['price_type']             = $value->price_type;
            $resp['main_image'] 		    = $value->main_image ? url('storage/'.$value->main_image) : '';
            $resp['floor_plan_image'] 		= optional($floorPlanImage)->floor_plan_image ? url('storage/'.$property->floor_plan_image) : '';
            $resp['allImages']              = $allImg;
            $resp['attributeDetails']       = $allAttribute;
            $allProperty[]                  =  $resp; 
        }
 
        return \Response::json(['data' => $allProperty, 'pagination' => $pagination, 'message'=>'Product List fetch successfully.', 'response_code' => 200], 200);
        
    } catch (Exception $e) {
        return Response::json(["line" => $e->getLine(), "message" => $e->getMessage()], 500);
    }

   }


   /**
    *Function Name : attributeList
    *Description : Attribute List
    *
    * @param user_id            User ID 
    *
    * @return response 
    */
   public function attributeList(Request $request){
   	try {

   		// Check if parameter exists
   		if(empty($request->user_id)){
   			return \Response::json(['message' => 'User Id required.', 'response_code' => 400], 400);
   		}

   		//Check User Delete
        if($this->userDeleted($request->get('user_id'))){
            return \Response::json(['message'=>'Your account has been deleted. Please contact to admin.', 'response_code' => 302], 302);
        }

        //Check User Block
        if($this->userBlocked($request->get('user_id'))){
            return \Response::json(['message'=>'You are blocked by admin. Please contact to admin.', 'response_code' => 303], 303);
        }

   		// Get all atrribute List 
   		$allAttribute = Attribute::all();

   		foreach ($allAttribute as $key => $value) {
   			$attr['attribute_id'] 	= $value->id;
   			$attr['attribute_name'] = $value->attribute_name;
   			$allAttr[]				= $attr;
   		}
   		return \Response::json(['data' => $allAttr, 'message' => 'Attribute List fetch successfully.', 'response_code' => 200], 200);
   	} catch (Exception $e) {
   		return Response::json(["line" => $e->getLine(), "message" => $e->getMessage()], 500);   		
   	}
   }

   /**
    *Function Name : followProperty
    *Description : Follow and Un follow property
    *
    * @param user_id            User ID 
    * @param property_id        Property ID 
    * @param type        		Type 1 for following and 2 for unfollow
    *
    * @return response 
    */
   public function followProperty(Request $request){
   	try {
   		// Check if parameter exists
   		if(empty($request->user_id) || empty($request->property_id)){
   			return \Response::json(['message' => 'All fileds are required.', 'response_code' => 400], 400);
   		}

   		//Check User Delete
        if($this->userDeleted($request->get('user_id'))){
            return \Response::json(['message'=>'Your account has been deleted. Please contact to admin.', 'response_code' => 302], 302);
        }

        //Check User Block
        if($this->userBlocked($request->get('user_id'))){
            return \Response::json(['message'=>'You are blocked by admin. Please contact to admin.', 'response_code' => 303], 303);
        }

        // Type 1 for following and 2 for unfollow
        if($request->type == '1'){
        	$checkFollow = PropertyFollow::where('property_id', $request->property_id)->where('user_id', $request->user_id)->first(); 

        	// check user already following this property
        	if($checkFollow){
        		return \Response::json(['message' => 'You are already following this property.', 'response_code' => 200], 200);
        	}else{

        		// Save data
				$addFollow 				= new PropertyFollow;
				$addFollow->property_id = $request->property_id;
				$addFollow->user_id 	= $request->user_id;
				$addFollow->save();
				return \Response::json(['message' => 'Property followed successfully.', 'response_code' => 200], 200);
        	}
	       
    	}elseif($request->type == '2'){
    		$checkFollow = PropertyFollow::where('property_id', $request->property_id)->where('user_id', $request->user_id)->first(); 

    		// check user already unfollowing this property
    		if($checkFollow){

    			// Delete data
    			$deleteFollow = PropertyFollow::find($checkFollow->id);
    			$deleteFollow->delete();
    			return \Response::json(['message' => 'Property unfollowed successfully.', 'response_code' => 200], 200);
    		}else{
    			return \Response::json(['message' => 'You are already unfollowing this property.', 'response_code' => 200], 200);
    		}
    	}else{
    		return \Response::json(['message' => 'Invalid type.', 'response_code' => 201], 201);
    	}

   		
   	} catch (Exception $e) {
   		return Response::json(["line" => $e->getLine(), "message" => $e->getMessage()], 500);
   	}
   }

   /**
    *Function Name : likeProperty
    *Description : Like and Unlike property
    *
    * @param user_id            User ID 
    * @param property_id        Property ID 
    * @param type        		Type 1 for like and 2 for unlike
    *
    * @return response 
    */
    public function likeProperty(Request $request){
   	try {
   		// Check if parameter exists
   		if(empty($request->user_id) || empty($request->property_id)){
   			return \Response::json(['message' => 'All fileds are required.', 'response_code' => 400], 400);
   		}

   		//Check User Delete
        if($this->userDeleted($request->get('user_id'))){
            return \Response::json(['message'=>'Your account has been deleted. Please contact to admin.', 'response_code' => 302], 302);
        }

        //Check User Block
        if($this->userBlocked($request->get('user_id'))){
            return \Response::json(['message'=>'You are blocked by admin. Please contact to admin.', 'response_code' => 303], 303);
        }

        // Type 1 for following and 2 for unfollow
        if($request->type == '1'){
        	$checkFollow = PropertyLike::where('property_id', $request->property_id)->where('user_id', $request->user_id)->first(); 

        	// check user already following this property
        	if($checkFollow){
        		return \Response::json(['message' => 'You are already liked this property.', 'response_code' => 200], 200);
        	}else{

        		// Save data
				$addFollow 				= new PropertyLike;
				$addFollow->property_id = $request->property_id;
				$addFollow->user_id 	= $request->user_id;
				$addFollow->save();
				return \Response::json(['message' => 'Property liked successfully.', 'response_code' => 200], 200);
        	}
	       
    	}elseif($request->type == '2'){
    		$checkFollow = PropertyLike::where('property_id', $request->property_id)->where('user_id', $request->user_id)->first(); 

    		// check user already unfollowing this property
    		if($checkFollow){

    			// Delete data
    			$deleteFollow = PropertyLike::find($checkFollow->id);
    			$deleteFollow->delete();
    			return \Response::json(['message' => 'Property dislike successfully.', 'response_code' => 200], 200);
    		}else{
    			return \Response::json(['message' => 'You are already dislike this property.', 'response_code' => 200], 200);
    		}
    	}else{
    		return \Response::json(['message' => 'Invalid type.', 'response_code' => 201], 201);
    	}

   		
   	} catch (Exception $e) {
   		return Response::json(["line" => $e->getLine(), "message" => $e->getMessage()], 500);
   	}
   }

   	/**
    *Function Name : commentProperty
    *Description : Comment property
    *
    * @param user_id            User ID 
    * @param property_id        Property ID 
    * @param comment            Comment on property 
    * @param Image              Comment of image 
    * @param type               Type 1 for comment and 2 for image
    *
    * @return response 
    */

   protected function commentProperty(Request $request)
   {

    try {
        // Check if parameter exists
        if(empty($request->user_id) || empty($request->property_id) || empty($request->type) || empty($request->comment) ) {
            return \Response::json(['message' => 'All fileds are required.', 'response_code' => 400], 400);
        }

        //Check User Delete
        if($this->userDeleted($request->user_id)){
            return \Response::json(['message'=>'Your account has been deleted. Please contact to admin.', 'response_code' => 302], 302);
        }

        //Check User Block
        if($this->userBlocked($request->user_id)){
            return \Response::json(['message'=>'You are blocked by admin. Please contact to admin.', 'response_code' => 303], 303);
        }
        // Check Type
        if($request->type == '1'){
        	// Add comment as text
            $addComment = new PropertyComment;
            $addComment->user_id = $request->user_id;
            $addComment->property_id = $request->property_id;
            $addComment->comment = $request->comment;
            $addComment->save();
            return \Response::json(['message'=>'Commented successfully.', 'response_code' => 200], 200);
        }else if($request->type == '2'){
        	// save comment images
        	if (!empty($request->comment)) {
	            $image = $request->comment;
	            $imageComment =  Storage::putFileAs('uploads/comment_images', $image, $image->getClientOriginalName());

        	}
        	// Add comment as Images
            $addComment = new PropertyComment;
            $addComment->user_id = $request->user_id;
            $addComment->property_id = $request->property_id;
            $addComment->image = $imageComment;
            $addComment->save();
            return \Response::json(['message'=>'Commented successfully.', 'response_code' => 200], 200);
        }else{
             return \Response::json(['message'=>'Invalid type.', 'response_code' => 201], 201);
        }


    } catch (Exception $e) {
        return Response::json(["line" => $e->getLine(), "message" => $e->getMessage()], 500);
    }

       
   }

      /**
    *Function Name : commentList
    *Description : Comment List
    *
    * @param user_id            User ID 
    * @param property_id        Property ID 
    *
    * @return response 
    */

   protected function commentList(Request $request)
   {

    try {
        // Check if parameter exists
        if(empty($request->user_id) || empty($request->property_id) || empty($request->page_size) || empty($request->page_number) ) {
            return \Response::json(['message' => 'All fileds are required.', 'response_code' => 400], 400);
        }

        //Check User Delete
        if($this->userDeleted($request->user_id)){
            return \Response::json(['message'=>'Your account has been deleted. Please contact to admin.', 'response_code' => 302], 302);
        }

        //Check User Block
        if($this->userBlocked($request->user_id)){
            return \Response::json(['message'=>'You are blocked by admin. Please contact to admin.', 'response_code' => 303], 303);
        }
        $allComment = array();
        // Count the comment
      	$commentList = PropertyComment::where('property_id', $request->property_id)->orderBy('id','DESC')->count();

      	/* Pagination Code Starts Here */
		$countData      = $commentList;
		$start_index    = ($request->page_number - 1) * $request->page_size;
		$Pages          = ($countData / $request->page_size);
		$maxPages       = ceil($Pages);

		$pagination['page_number']      = $request->page_number;
		$pagination['totalRecords']     = $countData;
		$pagination['maxPages']         = $maxPages;
		/* Pagination Code Ends Here */
		//
		//Get the comment with pagination 
		$commentList = PropertyComment::where('property_id', $request->property_id)->orderBy('id','DESC')->offset($start_index)->limit($request->page_size)->get();

      	foreach ($commentList as $key => $value) {
	      	$comment['user_details'] = User::where('id',$value->user_id)->first();
	      	if($value->comment){
	      		$comment['comment'] = $value->comment;
	      	}else{
	      		$comment['comment'] = url('storage/'.$value->image);
	      	}
	      	$comment['comment_id'] = $value->id;
	      	$comment['comment_date'] = strtotime($value->created_at);
	      	$allComment[] = $comment;
      	}

      	return \Response::json(['data' => $allComment,'pagination' => $pagination,'message'=>'Comment List fetch sucessfully.', 'response_code' => 200], 200);

    } catch (Exception $e) {
        return Response::json(["line" => $e->getLine(), "message" => $e->getMessage()], 500);
    }

       
   }

   	/**
    *Function Name : commentOnComment
    *Description : comment On Comment
    *
    * @param user_id            User ID 
    * @param property_id        Property ID 
    * @param comment_id         Comment ID 
    * @param comment            Comment on property 
    * @param Image              Comment of image 
    * @param type               Type 1 for comment and 2 for image
    *
    * @return response 
    */

   protected function commentOnComment(Request $request)
   {

    try {
        // Check if parameter exists
        if(empty($request->user_id) || empty($request->property_id) || empty($request->type) || empty($request->comment) ) {
            return \Response::json(['message' => 'All fileds are required.', 'response_code' => 400], 400);
        }

        //Check User Delete
        if($this->userDeleted($request->user_id)){
            return \Response::json(['message'=>'Your account has been deleted. Please contact to admin.', 'response_code' => 302], 302);
        }

        //Check User Block
        if($this->userBlocked($request->user_id)){
            return \Response::json(['message'=>'You are blocked by admin. Please contact to admin.', 'response_code' => 303], 303);
        }
        // Check Type
        if($request->type == '1'){
        	// Add comment as text
            $addComment 				= new PropertyComment;
            $addComment->user_id 		= $request->user_id;
            $addComment->property_id 	= $request->property_id;
            $addComment->comment 		= $request->comment;
            $addComment->parent_id 		= $request->comment_id;
            $addComment->save();
            return \Response::json(['message'=>'Commented successfully.', 'response_code' => 200], 200);
        }else if($request->type == '2'){
        	// save comment images
        	if (!empty($request->comment)) {
	            $image = $request->comment;
	            $imageComment =  Storage::putFileAs('uploads/comment_images', $image, $image->getClientOriginalName());

        	}
        	// Add comment as Images
            $addComment 				= new PropertyComment;
            $addComment->user_id 		= $request->user_id;
            $addComment->property_id 	= $request->property_id;
            $addComment->image  		= $imageComment;
            $addComment->parent_id 		= $request->comment_id;
            $addComment->save();
            return \Response::json(['message'=>'Commented successfully.', 'response_code' => 200], 200);
        }else{
             return \Response::json(['message'=>'Invalid type.', 'response_code' => 201], 201);
        }


    } catch (Exception $e) {
        return Response::json(["line" => $e->getLine(), "message" => $e->getMessage()], 500);
    }

       
   }


    /**
     * Function Name : userBlocked
     * Description   : Check User Block or Not
     *
     * @param XYZ $type    user_id
     *
     * @return response
     */
    public function userBlocked($user_id)
    {
        try { 

            $userBlock = User::where('id', $user_id)->first();
            //Check User blocked or not
            if($userBlock->is_blocked == 1){
            	return 1;
            }
           
        }
        catch (Exception $e) {
            return Response::json(["line" => $e->getLine(), "msg" => $e->getMessage()], 500);
        }  
    }


    /**
     * Function Name : userDeleted
     * Description   : Check User Delete or Not
     *
     * @param XYZ $type    user_id
     *
     * @return response
     */
    public function userDeleted($user_id)
    {
        try { 

            $userBlock = User::where('id', $user_id)->first();

            //Check User deleted or not
            if($userBlock->is_deleted == 1){
            	return 1;
            }
           
        }
        catch (Exception $e) {
            return Response::json(["line" => $e->getLine(), "msg" => $e->getMessage()], 500);
        }  
    }
}
