<?php
/**
 * Dashboard File Document Comment
 *
 * PHP Version 7.1
 *
 * @category Controllers
 * @package  Controller
 * @author   Mobiloitte <it@mobiloitte.in>
 * @license  http://mobiloitte.com/ GNU General Public License
 * @link     app/Http/Controllers/Api
 */
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\UserDevice;
use App\User;
use App\UserPasswordVerification;
use App\UserNeighbour;
use App\Property;
use App\Tutorial;
use App\FollowFollowing;
use App\AppReview;
use App\StaticContent;
use App\UserRole;
use App\Mail\ForgotPassword;
use Illuminate\Support\Facades\Auth;
use Response;
use Mail;
use Hash;
use Exception;
use Carbon\Carbon;
use DB;
use App\Http\Controllers\Helper\Helper;

/**
 * Company Class Doc Comment
 *
 * @category Class
 * @package  Controller
 * @author   Akaash <akaash.mehra@mobiloitte.in>
 * @license  http://mobiloitte.com/ GNU General Public License
 * @link     app/Http/Controllers/Api
 */

class ApiController extends Controller
{

    /**
     * Function Name : userlogin
     * Description   : Login
     *
     * @param  $email       email
     * @param  $password    password
     *
     * @return response
     */
    public function userlogin(Request $request)
    {
    	try { 

    		// Check if parameter exists
	    	if (empty($request->get('email')) || empty($request->get('password'))) {
	    		return \Response::json(['message' => 'Please provide email and password.', 'response_code' => 400], 400);
	    	} 



	    	// Get user details from DB table
	        $where = array(
	            'email'    => $request->get('email'),
	            'password' => $request->get('password'),
	        );

	        $user = Auth::attempt($where);

	        // Check if user exists
	        if ($user) {
	        	
	        	// Loading user data
	        	$user = Auth::user();

	        	//Check User Delete
	        	if($this->userDeleted($user->id)){
	        		return \Response::json(['message'=>'Your account has been deleted. Please contact to admin.', 'response_code' => 302], 302);
	        	}

	        	//Check User Block
	        	if($this->userBlocked($user->id)){
	        		return \Response::json(['message'=>'You are blocked by admin. Please contact to admin.', 'response_code' => 303], 303);
	        	}

	        	$resp                   = array();
                $resp['user_id']        = $user->id;
                $resp['full_name']      = $user->name;
                $resp['phone_number']   = ($user->phone_number) ? $user->phone_number : "";
                $resp['country_code']   = ($user->country_code) ? $user->country_code : "";
                $resp['gender']         = $user->gender;
                $resp['email']          = $user->email;
                $resp['class']          = ($user->class) ? $user->class : "";
                $resp['date_of_birth']  = ($user->date_of_birth) ? $user->date_of_birth : "";
                $resp['cover_image']    = ($user->cover_image) ? url('storage/'.$user->cover_image) : "";
                $resp['profile_image']  = ($user->profile_image) ? url('storage/'.$user->profile_image) : "";
                $resp['latitude']       = ($user->latitude) ? $user->latitude : "";
                $resp['longitude']      = ($user->longitude) ? $user->longitude : "";
                $resp['apartment']      = ($user->apartment) ? $user->apartment : "";
                $resp['street_address'] = ($user->street_address) ? $user->street_address : "";
                $resp['documents']      = ($user->documents) ? $user->documents : "";
                $resp['status']         = $user->address_verified;
                $resp['notification_status']  = $user->notification_status;
                $resp['token']          = $user->createToken('AssetsCo')->accessToken;

	        	// Check if device info exists
	        	if (!empty($request->get('device_token')) || !empty($request->get('device_type')) ) {
	        		
	        		// Saving users device info
	        		$userDevice = UserDevice::create([
	        			'user_id'      => $user->id,
	        			'device_token' => $request->get('device_token'),
	        			'device_type'  => $request->get('device_type'),
	        		]);
	        	}
                


	        	// Response
	        	return \Response::json(['data' => $resp, 'message'=>'User login successfully', 'response_code' => 200], 200);

	        } else {
	        	return \Response::json(['message' => 'Email or password not registered.', 'response_code' => 401], 401);
	        }
    	} catch(Exception $e) {
    		return Response::json(["line" => $e->getLine(), "msg" => $e->getMessage()], 500);
    	}
    }


    /**
     * Function Name : signup
     * Description   : Signup
     *
     * @param  $full_name         full_name
     * @param  $email             email
     * @param  $phone_number      phone_number
     * @param  $gender            gender
     * @param  $password          password
     * @param  $street_address    address
     * @param  $apartment         apartment
     * @param  $latitude          latitude
     * @param  $longitude         longitude
     * @param  $device_token      device_token
     * @param  $device_type       device_type
     * @param  $referral_code     referral_code
     *
     * @return response
     */
    public function signup(Request $request)
    {
    	try { 

    		// Check if parameter exists
	    	if (empty($request->get('full_name')) || empty($request->get('email')) || empty($request->get('phone_number')) || empty($request->get('password')) ) {
	    		return \Response::json(['message' => 'All fields are required.', 'response_code' => 400], 400);
	    	} 

        	// Check if email already exist
            $userEmail = User::where('email', $request->get('email'))->first();
            if ($userEmail) {
            	return \Response::json(['message'=>'Email already exists.', 'response_code' => 401], 401);
            } 

            // Check if phonenumber already exist
            $userPhone = User::where('phone_number', $request->get('phone_number'))->first();
            if ($userPhone) {
	            return \Response::json(['message'=>'Phonenumber already exists.', 'response_code' => 401], 401);
            }
            $profile_image =  $request->profile_image;
            $cover_image =  $request->cover_image;
          
            $imageProfileUpload = '';
            $imageCoverUpload = '';
            // Profile Image Check
            if (!empty($profile_image)) {
                $image = $request->profile_image;
                $imageProfileUpload =  Storage::putFileAs('uploads/profile_images', $image, $image->getClientOriginalName());
            }
             // Cover Image Check
            if (!empty($cover_image)) {
                $image = $request->cover_image;
                $imageCoverUpload =  Storage::putFileAs('uploads/profile_images', $image, $image->getClientOriginalName());
            }
            // Saving users data
               $userSave                    = new User;
               $userSave->name              = $request->full_name;
               $userSave->email             = $request->email;
               $userSave->password          = bcrypt($request->password);
               $userSave->gender            = $request->gender;
               $userSave->phone_number      = $request->phone_number;
               $userSave->country_code      = $request->country_code;
               $userSave->save();

               $userRole                   = new UserRole;
               $userRole->user_id          = $userSave->id;
               $userRole->role_id          = '2';
               $userRole->save();

                 
            // Check if device parameter exists
            if (!empty($request->get('device_token')) || !empty($request->get('device_type'))) {
            	$userDevice = UserDevice::create([
            		'user_id'      => $userSave->id,
            		'device_token' => $request->get('device_token'),
            		'device_type'  => $request->get('device_type'),
            	]);
            }

            // Response data
            $resp                   = array();
            $resp['user_id']        = $userSave->id;
            $resp['full_name']      = $userSave->name;
            $resp['phone_number']   = $userSave->phone_number;
            $resp['country_code']   = $userSave->country_code;
            $resp['gender']         = $userSave->gender;
            $resp['email']          = $userSave->email;
            $resp['status']         = ($userSave->address_verified) ? $userSave->address_verified : "";
            $resp['token']          = $userSave->createToken('AssetsCo')->accessToken;

            return \Response::json(['data' => $resp, 'message'=>'User registered successfully.', 'response_code' => 200], 200);

    	} catch(Exception $e) {
    		return Response::json(["line" => $e->getLine(), "message" => $e->getMessage()], 500);
    	}
    }


    /**
     * Function Name : socialLogin
     * Description   : Social Login Api
     *
     * @param  $fullname        fullname
     * @param  $username        userName     
     * @param  $email           email
     * @param  $phone_number    phone_number
     * @param  $social_id       social_id
     * @param  $social_type     social_type
     * @param  $deviceToken     deviceToken
     * @param  $deviceType      deviceType
     * @param  $gender          gender
     * @param  $date_of_birth   date_of_birth
     *
     * @return response
     */
    public function socialLogin(Request $request)
    {
        try {
            $fullname      = $request->get('full_name');
            $social_id     = $request->get('social_id');
            $phone_number  = $request->get('phone_number');
            $country_code  = $request->get('country_code');
            $social_type   = $request->get('social_type');
            $email         = $request->get('email');
            $deviceToken   = $request->get('deviceToken');
            $deviceType    = $request->get('deviceType');
            $userimage     = $request->get('userimage');
            $gender        = $request->get('gender');
            $date_of_birth = $request->get('date_of_birth');
            $profile_image = $request->get('profile_image');

             

            // Check if parameter exists
            if ( empty($social_id) || empty($social_type) )  {
                return \Response::json(['message' => 'All fields are required.', 'response_code' => 400], 400);
            } 

             // Check if social Id and Socail type exist
            if(!empty($social_id) && !empty($social_type)) { 

               
                $getSocialUser      = User::where(['social_id' => $social_id , 'social_type' => $social_type])->first();
                $getSocialUserEmail = User::where(['email' => $email])->first();


                // Check if User exist
                if(count($getSocialUser) > 0) {
                  //Check User Delete
                 if($this->userDeleted($getSocialUser->id)){
                    return \Response::json(['message'=>'Your account has been deleted. Please contact to admin.', 'response_code' => 302], 302);
                 }

                 //Check User Block
                 if($this->userBlocked($getSocialUser->id)){
                    return \Response::json(['message'=>'You are blocked by admin. Please contact to admin.', 'response_code' => 303], 303);
                 }
 
                    // Check if device parameter exists
                    if(!empty($deviceToken) && $deviceType != "") {

                        $checkDevice =  UserDevice::where('user_id', $getSocialUser->id)->where('device_token',$deviceToken)->where('device_type',$deviceType)->get()->toArray();


                        // Check Devide type and device ID exist or not
                        if(count($checkDevice) == 0) {

                            // Save device token and Device Id
                            $userDevice = UserDevice::create([
                                'user_id'      => $getSocialUser->id,
                                'device_token' => $request->get('device_token'),
                                'device_type'  => $request->get('device_type'),
                            ]);
                        }
                    }
                    // print_r($getSocialUser);exit;
                    /* Response data */
                    $resp                   = array();
                    $resp['user_id']        = $getSocialUser->id;
                    $resp['social_id']      = $getSocialUser->social_id;
                    $resp['full_name']      = ($getSocialUser->name) ? $getSocialUser->name : "";
                    $resp['username']       = ($getSocialUser->username) ? $getSocialUser->username : "";
                    $resp['email']          = ($getSocialUser->email) ? $getSocialUser->email : "";
                    $resp['phone_number']   = ($getSocialUser->phone_number) ? $getSocialUser->phone_number : "";
                    $resp['country_code']   = ($getSocialUser->country_code) ? $getSocialUser->country_code : "";
                    $resp['gender']         = ($getSocialUser->gender) ? $getSocialUser->gender : "";
                    $resp['date_of_birth']  = ($getSocialUser->date_of_birth) ? $getSocialUser->date_of_birth : "";
                     // Check If profile image with Social signup
                    if (filter_var($getSocialUser->profile_image, FILTER_VALIDATE_URL)) {
                        $resp['profile_image']  = ($getSocialUser->profile_image) ? $getSocialUser->profile_image : "";
                    }else{
                        $resp['profile_image']  = ($getSocialUser->profile_image) ? url('storage/'.$getSocialUser->profile_image) : "";
                    }
                    $resp['cover_image']    = ($getSocialUser->cover_image) ? url('storage/'.$getSocialUser->cover_image) : "";
                    $resp['latitude']       = ($getSocialUser->latitude) ? $getSocialUser->latitude : "";
                    $resp['longitude']      = ($getSocialUser->longitude) ? $getSocialUser->longitude : "";
                    $resp['apartment']      = ($getSocialUser->apartment) ? $getSocialUser->apartment : "";
                    $resp['street_address'] = ($getSocialUser->street_address) ? $getSocialUser->street_address : "";
                    $resp['status']         = ($getSocialUser->address_verified) ? $getSocialUser->address_verified : "";
                    $resp['notification_status']  = $getSocialUser->notification_status;
                     $resp['documents']      = ($getSocialUser->documents) ? $getSocialUser->documents : "";
                    $resp['token']          = $getSocialUser->createToken('AssetsCo')->accessToken;

                    return array( 'data' => $resp, 'response_code' => 200, 'message' => 'User login successfully');
                } 
                // Check if  Email Exist
                elseif (isset($getSocialUserEmail)) {
                  
                     // Check if device parameter exists
                    if(!empty($deviceToken) && $deviceType != "") {

                        $checkDevice =  UserDevice::where('user_id',$getSocialUserEmail->id)->where('device_token',$deviceToken)->where('device_type',$deviceType)->get();

                        // Check Devide type and device ID exist or not
                        if(count($checkDevice) == 0) {
                            // Save device token and Device Id
                            $userDevice = UserDevice::create([
                                'user_id'      => $getSocialUser->id,
                                'device_token' => $request->get('device_token'),
                                'device_type'  => $request->get('device_type'),
                            ]);
                        }
                    }
                    $updatePassword   = User::where('id', $getSocialUserEmail->id)->update(['social_id' => $social_id, 'social_type' => $social_type, 'password' => bcrypt($social_id)]);
                    $resp                   = array();
                    $resp['user_id']        = $getSocialUser->id;
                    $resp['social_id']      = $getSocialUser->social_id;
                    $resp['full_name']      = ($getSocialUser->name) ? $getSocialUser->name : "";
                    $resp['username']       = ($getSocialUser->username) ? $getSocialUser->username : "";
                    $resp['email']          = ($getSocialUser->email) ? $getSocialUser->email : "";
                    $resp['phone_number']   = ($getSocialUser->phone_number) ? $getSocialUser->phone_number : "";
                    $resp['country_code']   = ($getSocialUser->country_code) ? $getSocialUser->country_code : "";
                    $resp['gender']         = ($getSocialUser->gender) ? $getSocialUser->gender : "";
                    $resp['date_of_birth']  = ($getSocialUser->date_of_birth) ? $getSocialUser->date_of_birth : "";
                    // Check If profile image with Social signup
                    if (filter_var($getSocialUser->profile_image, FILTER_VALIDATE_URL)) {
                        $resp['profile_image']  = ($getSocialUser->profile_image) ? $getSocialUser->profile_image : "";
                    }else{
                        $resp['profile_image']  = ($getSocialUser->profile_image) ? url('storage/'.$getSocialUser->profile_image) : "";
                    }

                    $resp['cover_image']    = ($getSocialUser->cover_image) ? url('storage/'.$getSocialUser->cover_image) : "";
                    $resp['latitude']       = ($getSocialUser->latitude) ? $getSocialUser->latitude : "";
                    $resp['longitude']      = ($getSocialUser->longitude) ? $getSocialUser->longitude : "";
                    $resp['apartment']      = ($getSocialUser->apartment) ? $getSocialUser->apartment : "";
                    $resp['street_address'] = ($getSocialUser->street_address) ? $getSocialUser->street_address : "";
                    $resp['status']         = ($getSocialUser->address_verified) ? $getSocialUser->address_verified : "";
                    $resp['notification_status']  = ($getSocialUser->notification_status);
                    $resp['documents']      = ($getSocialUser->documents) ? $getSocialUser->documents : "";
                    $resp['token']          = $getSocialUser->createToken('AssetsCo')->accessToken;
                    return array('response_code' => 200, 'message' => 'User login successfully' , 'data' => $resp);
                } 
                // Check if new user
                else {

                    // Check if phonenumber already exist
                  
                     
                    $data = array(
                        'name'          => $fullname,
                        'email'         => $email,
                        'phone_number'  => $phone_number,
                        'country_code'  => $country_code,
                        'social_id'     => $social_id,
                        'social_type'   => $social_type,
                        'gender'        => $gender,
                        'date_of_birth' => $date_of_birth,
                        'profile_image' => $profile_image,
                        'password'      => bcrypt($social_id)
                    );
                    $user_id = User::insertGetId($data); // Get last insert id.

                    $userRole                   = new UserRole;
                    $userRole->user_id          = $user_id;
                    $userRole->role_id          = '2';
                    $userRole->save();
                    
                    // Check if device parameter exists
                    if (isset($request->device_token) && isset($request->device_type)) {

                         // Save Device Type and Device Token 
                        $userDevice = UserDevice::create([
                                'user_id'      => $user_id,
                                'device_token' => $request->get('device_token'),
                                'device_type'  => $request->get('device_type'),
                        ]);
                    }

                    /*Insert setting*/
                    $user                           = User::find($user_id);
                   

                    /* Response data */
                    $resp                   = array();
                    $resp['user_id']        = $user_id;
                    $resp['social_id']      = $user->social_id;
                    $resp['full_name']      = ($user->name) ? $user->name : "";
                    $resp['username']       = ($user->username) ? $user->username : "";
                    $resp['email']          = ($user->email) ? $user->email : "";
                    $resp['country_code']   = ($user->country_code) ? $user->country_code : "";
                    $resp['phone_number']   = ($user->phone_number) ? $user->phone_number : "";
                    $resp['gender']         = ($user->gender) ? $user->gender : "";
                    $resp['date_of_birth']  = ($user->date_of_birth) ? $user->date_of_birth : "";
                    // Check If profile image with Social signup
                    if (filter_var($user->profile_image, FILTER_VALIDATE_URL)) {
                        $resp['profile_image']  = ($user->profile_image) ? $user->profile_image : "";
                    }else{
                        $resp['profile_image']  = ($user->profile_image) ? url('storage/'.$user->profile_image) : "";
                    }
                    $resp['cover_image']    = ($user->cover_image) ? url('storage/'.$user->cover_image) : "";
                    $resp['latitude']       = ($user->latitude) ? $user->latitude : "";
                    $resp['longitude']      = ($user->longitude) ? $user->longitude : "";
                    $resp['apartment']      = ($user->apartment) ? $user->apartment : "";
                    $resp['street_address'] = ($user->street_address) ? $user->street_address : "";
                    $resp['status']         = ($user->address_verified) ? $user->address_verified : "";
                    $resp['documents']      = ($user->documents) ? $user->documents : "";
                    $resp['notification_status']        = ($user->notification_status);
                    $resp['token']          = $user->createToken('AssetsCo')->accessToken;
                    return array('response_code' => 200, 'message' => 'User registered successfully.', 'user_id' => $user_id, 'data' => $resp, 'type' => 'newSignup');
                }
            } else {
                return array('response_code' => 400, 'message' => 'Please provide all fields');
            }
        } catch(Exception $e){
            return json_encode(array('response_code' => 500, 'message' => $e->getMessage() ,'line'=>$e->getLine()));
        }
    }

     

    /**
     * Function Name : getProfile
     * Description   : Get Profile Api
     *
     * @param  $user_id    user_id
     *
     * @return response
     */
    public function getProfile(Request $request)
    {
        try { 

            // Check if parameter exists
            if (empty($request->get('user_id'))) {
                return \Response::json(['message' => 'User id is required.', 'response_code' => 400], 400);
            } 

            // Load user from user id
            $user = User::where('id', $request->get('user_id'))->first();

            // Check if user exists
            if ($user) {

            	//Check User Delete
	        	if($this->userDeleted($user->id)){
	        		return \Response::json(['message'=>'Your account has been deleted. Please contact to admin.', 'response_code' => 302], 302);
	        	}

	        	//Check User Block
	        	if($this->userBlocked($user->id)){
	        		return \Response::json(['message'=>'You are blocked by admin. Please contact to admin.', 'response_code' => 303], 303);
	        	}

                $resp                    = array();
                $resp['user_id']         = $user->id;
                $resp['full_name']       = $user->name;
                $resp['phone_number']    = $user->phone_number;
                $resp['gender']          = $user->gender;
                $resp['email']           = $user->email;
                $resp['class']           = ($user->class) ? $user->class : "";
                $resp['date_of_birth']   = ($user->date_of_birth) ? $user->date_of_birth : "";
                $resp['cover_image']     = ($user->cover_image) ? url('storage/'.$user->cover_image) : "";
                // Check If profile image with Social signup
                if (filter_var($user->profile_image, FILTER_VALIDATE_URL)) {
                    $resp['profile_image']  = ($user->profile_image) ? $user->profile_image : "";
                }else{
                    $resp['profile_image']  = ($user->profile_image) ? url('storage/'.$user->profile_image) : "";
                }
                $resp['latitude']        = ($user->latitude) ? $user->latitude : "";
                $resp['longitude']       = ($user->longitude) ? $user->longitude : "";
                $resp['apartment']       = ($user->apartment) ? $user->apartment : "";
                $resp['street_address']  = ($user->street_address) ? $user->street_address : "";
                $resp['zip_code']        = ($user->zip_code) ? $user->zip_code : "";
                $resp['social_id']        = ($user->social_id) ? $user->social_id : "";
                $resp['notification_status']        = $user->notification_status;
                $resp['purchase_count']  = 0;

                // Response
                return \Response::json(['data' => $resp, 'message'=>'User details fetched successfully.', 'response_code' => 200], 200);

            } else {
                return \Response::json(['message' => 'User does not exists.', 'response_code' => 401], 401);
            }
        } catch(Exception $e) {
            return Response::json(["line" => $e->getLine(), "message" => $e->getMessage()], 500);
        }
    }


    /**
     * Function Name : forgotPassword
     * Description   : Forgot Password Api
     *
     * @param  $email    email
     *
     * @return response
     */
    public function forgotPassword(Request $request)
    {
        try { 

            // Check if parameter exist
           if ( empty($request->get('email')) ) {
                return \Response::json(['message' => 'Email is required.', 'response_code' => 400], 400);
           } 

            //Get user Id 
            $user_id = User::where(['email' => $request->get('email')])->first();

            //Check User Delete
            if($this->userDeleted($user_id->id)){
                return \Response::json(['message'=>'Your account has been deleted. Please contact to admin.', 'response_code' => 302], 302);
            }

            //Check User Block
            if($this->userBlocked($user_id->id)){
                return \Response::json(['message'=>'You are blocked by admin. Please contact to admin.', 'response_code' => 303], 303);
            }
            // Check If user exist
            if($user_id){

               //create a new token to be sent to the user. 
                DB::table('password_resets')->insert([
                    'email' => $request->email,
                    'token' => str_random(60), //change 60 to any length you want
                    'created_at' => Carbon::now()
                ]);

                $tokenData = DB::table('password_resets')
                ->where('email', $request->email)->first();

                $token = $tokenData->token;
                $email = $request->email; 

                $data = ['url'=>url('reset-password/' . $token),'email'=>$request->email,'token'=>$token];

                //Send Mail
                Mail::send('mail_template.reset_mail', $data, function($message) use ($data) {
                 $message->to($data['email'])->subject('Reset Password Link');
                });

                // Response
                return \Response::json(['message'=>'Link sent successfully to registered email address.', 'response_code' => 200], 200);

            } else {
                return \Response::json(['message' => 'User does not exists.', 'response_code' => 401], 401);
            }
        } catch(Exception $e) {
            return Response::json(["line" => $e->getLine(), "message" => $e->getMessage()], 500);
        }
    }


    /**
     * Function Name : usersNeighbourhood
     * Description   : Users Neighbourhood Api
     *
     * @param  $user_id              user_id
     * @param  $neighbour_details    neighbour_details
     *
     * @return response
     */
    public function usersNeighbourhood(Request $request)
    {
        try { 

            // Check if parameter exists
            if (empty($request->get('user_id'))) {
                return \Response::json(['message' => 'User id is required.', 'response_code' => 400], 400);
            } 

            // Check if neighbour hood atleast 3
            if ( empty(count($request->get('neighbour_details')) < 3) ) {
                return \Response::json(['message' => 'Please select atleast 3 neighbourhoods.', 'response_code' => 401], 401);
            } 

            // Load user from user id
            $user = User::where('id', $request->get('user_id'))->first();

            // Check if user exists
            if ($user) {

                foreach ($request->get('neighbour_details') as $key => $value) {

                	UserNeighbour::create([
                		'user_id'   => $request->get('user_id'),
                		'name'      => $value['name'],
                		'latitude'  => $value['latitude'],
                		'longitude' => $value['longitude'],
                		'image'     => $value['image'],
                	]);
                }

                // Response
                return \Response::json(['message'=>'Neighbourhood added successfully.', 'response_code' => 200], 200);

            } else {

                return \Response::json(['message' => 'User does not exists.', 'response_code' => 401], 401);
            }
        } catch(Exception $e) {
            return Response::json(["line" => $e->getLine(), "message" => $e->getMessage()], 500);
        }
    }


    /**
     * Function Name : changePassword
     * Description   : Change Password Api
     *
     * @param  $user_id         user_id
     * @param  $old_password    old_password
     * @param  $new_password    new_password
     *
     * @return response
     */
    public function changePassword(Request $request)
    {
        try { 

            // Check if parameter exists
            if (empty($request->get('user_id'))) {
                return \Response::json(['message' => 'User id is required.', 'response_code' => 400], 400);
            } 

            //Check User Delete
        	if($this->userDeleted($request->get('user_id'))){
        		return \Response::json(['message'=>'Your account has been deleted. Please contact to admin.', 'response_code' => 302], 302);
        	}

        	//Check User Block
        	if($this->userBlocked($request->get('user_id'))){
        		return \Response::json(['message'=>'You are blocked by admin. Please contact to admin.', 'response_code' => 303], 303);
        	}

            // Load user from user id
            $user = User::where('id', $request->get('user_id'))->first();

            // Check if user exists
            if ($user) {

                if (Hash::check($request->get('old_password'), $user->password)) {

                	$user->password = bcrypt($request->get('new_password'));
                	$user->save();

				    // Response
                	return \Response::json(['message'=>'Password updated successfully.', 'response_code' => 200], 200);
				} else {

					return \Response::json(['message' => 'Please enter valid old password.', 'response_code' => 401], 401);
				}
            } else {

                return \Response::json(['message' => 'User does not exists.', 'response_code' => 401], 401);
            }
        } catch(Exception $e) {

            return Response::json(["line" => $e->getLine(), "message" => $e->getMessage()], 500);
        }
    }


    /**
     * Function Name : editProfile
     * Description   : Edit Profile Api
     *
     * @param  $user_id           user_id
     * @param  $profile_image     profile_image
     * @param  $cover_image       cover_image
     * @param  $full_name         full_name
     * @param  $email             email
     * @param  $phone_number      phone_number
     * @param  $gender            gender
     * @param  $street_address    street_address
     * @param  $apartment         apartment
     * @param  $date_of_birth     date_of_birth
     *
     * @return response
     */
    public function editProfile(Request $request)
    {
        try { 

            // Check if parameter exists
            if (empty($request->get('user_id'))) {
                return \Response::json(['message' => 'User id is required.', 'response_code' => 400], 400);
            } 

            // Load user from user id
            $user = User::where('id', $request->get('user_id'))->first();

            // Check if user exists
            if ($user) {

	            //Check User Delete
	        	if($this->userDeleted($request->get('user_id'))){
	        		return \Response::json(['message'=>'Your account has been deleted. Please contact to admin.', 'response_code' => 302], 302);
	        	}

	        	//Check User Block
	        	if($this->userBlocked($request->get('user_id'))){
	        		return \Response::json(['message'=>'You are blocked by admin. Please contact to admin.', 'response_code' => 303], 303);
	        	}

            	// Check if email exists with other user
            	$emailExists = User::where('email', $request->get('email'))->where('id', '!=', $request->get('user_id'))->first();
            	if ($emailExists) {
            		return \Response::json(['message' => 'Email already exists.', 'response_code' => 401], 401);
            	}

            	// Check if number exists with other user
            	$numberExists = User::where('email', $request->get('phone_number'))->where('id', '!=', $request->get('user_id'))->first();
            	if ($numberExists) {
            		return \Response::json(['message' => 'Phonenumber already exists.', 'response_code' => 401], 401);
            	}

            	// Checking if profile image exists
            	$profileImageName = '';
            	if (isset($request->profile_image)) {
                    $profileImageName = 'image-'.time().'.'.$request->profile_image->getClientOriginalExtension();
                    $request->profile_image->move(public_path('uploads/users'), $profileImageName);
                }

                // Checking if profile image exists
            	$coverImageName = '';
            	if (isset($request->cover_image)) {
                    $coverImageName = 'image-'.time().'.'.$request->cover_image->getClientOriginalExtension();
                    $request->cover_image->move(public_path('uploads/users'), $coverImageName);
                }



                $profile_image =  $request->profile_image;
                $cover_image =  $request->cover_image;

                $imageProfileUpload = '';
                $imageCoverUpload = '';
                // Profile Image Check
                if (!empty($profile_image)) {
                    $image = $request->profile_image;
                    $imageProfileUpload =  Storage::putFileAs('uploads/profile_images', $image, $image->getClientOriginalName());
                }
                // Cover Image Check
                if (!empty($cover_image)) {
                    $image = $request->cover_image;
                    $imageCoverUpload =  Storage::putFileAs('uploads/profile_images', $image, $image->getClientOriginalName());
                }

            	// Updating users profile
            	$user->name           = ($request->get('full_name')) ? $request->get('full_name') : $user->name;
            	$user->email          = ($request->get('email')) ? $request->get('email') : $user->email;
            	$user->date_of_birth  = ($request->get('date_of_birth')) ? $request->get('date_of_birth') : $user->date_of_birth;
            	$user->gender         = ($request->get('gender')) ? $request->get('gender') : $user->gender;
            	$user->phone_number   = ($request->get('phone_number')) ? $request->get('phone_number') : $user->phone_number;
            	$user->apartment      = ($request->get('apartment')) ? $request->get('apartment') : $user->apartment;
            	$user->street_address = ($request->get('street_address')) ? $request->get('street_address') : $user->street_address;
            	$user->profile_image  = ($imageProfileUpload) ? $imageProfileUpload : $user->profile_image;
                $user->cover_image    = ($imageCoverUpload) ? $imageCoverUpload : $user->cover_image;
                $user->zip_code       = ($request->get('zip_code')) ? $request->get('zip_code') : $user->name;
            	$user->address_verified = 1;
            	$user->save();

            	// Creating response
            	$user = User::where('id', $request->get('user_id'))->first();

                // Count of property
                $forRent = Property::where('user_id', $user->id)->where('property_for', 1)->count();
                $forSale = Property::where('user_id', $user->id)->where('property_for', 2)->count();

                // Count of followers and followings
                $followerCount  = FollowFollowing::where('followed_to', $user->id)->count();
                $followingCount = FollowFollowing::where('followed_by', $user->id)->count();

                //Count of neighbourhoods
                $neighbourCount = UserNeighbour::where('user_id', $user->id)->count();

            	$resp                    = array();
            	$resp['user_id']         = $user->id;
                $resp['full_name']       = $user->name;
                $resp['phone_number']    = $user->phone_number;
                $resp['gender']          = $user->gender;
                $resp['email']           = $user->email;
                $resp['class']           = ($user->class) ? $user->class : "";
                $resp['date_of_birth']   = ($user->date_of_birth) ? $user->date_of_birth : "";
                $resp['cover_image']     = ($user->cover_image) ? url('uploads/users/'.$user->cover_image) : "";
                // Check If profile image with Social signup
                if (filter_var($user->profile_image, FILTER_VALIDATE_URL)) {
                    $resp['profile_image']  = ($user->profile_image) ? $user->profile_image : "";
                }else{
                    $resp['profile_image']  = ($user->profile_image) ? url('storage/'.$user->profile_image) : "";
                }
                $resp['latitude']        = ($user->latitude) ? $user->latitude : "";
                $resp['longitude']       = ($user->longitude) ? $user->longitude : "";
                $resp['apartment']       = ($user->apartment) ? $user->apartment : "";
                $resp['street_address']  = ($user->street_address) ? $user->street_address : "";
                $resp['zip_code']        = ($user->zip_code) ? $user->zip_code : "";
                $resp['address_verified'] = ($user->address_verified) ? $user->address_verified : "";
            

			    // Response
            	return \Response::json(['data' => $resp, 'message'=>'Profile updated successfully.', 'response_code' => 200], 200);

            } else {

                return \Response::json(['message' => 'User does not exists.', 'response_code' => 401], 401);
            }
        } catch(Exception $e) {
            return Response::json(["line" => $e->getLine(), "message" => $e->getMessage()], 500);
        }
    }

     /**
     * Function Name : tutorials
     * Description   : tutorials List
     *
     * @param 
     *
     * @return response
     */
    public function tutorials()
    {
        try { 

        	$arrData = array();

        	// fetch all data of tutorials
        	$allData = Tutorial::all();
        	foreach ($allData as $key => $value) {
        		$arr['id'] = $value->id;
        		$arr['tut_image'] = url('storage/'.$value->tut_image);
        		$arr['description'] = $value->description;
        		$arrData[] = $arr;
        	}
           
        	// Check If tutorials 
            if ($arrData){

            	// Response
                return \Response::json(['data' => $arrData, 'message'=>'Tutorial list fetched successfully.', 'response_code' => 200], 200);
            } else {

                return \Response::json(['data' => $allData,'message'=>'No data found.', 'response_code' => 401], 401);
            }
        }
        catch (Exception $e) {
            return Response::json(["line"=>$e->getLine(),"message"=>$e->getMessage()], 500);
        }  
    }

    /**
     * Function Name : followUser
     * Description   : Follow user Api
     *
     * @param  $follow_to    follow_to
     * @param  $follow_by    follow_by
     *
     * @return response
     */
    public function followUser(Request $request)
    {
        try { 

            // Check parameter
            if ( empty($request->get('follow_to')) || empty($request->get('follow_by')) ) {
                return \Response::json(['message' => 'All fields are required.', 'response_code' => 400], 400);
            } else {

                $followTo = User::where('id', $request->get('follow_to'))->first();
                $followBy = User::where('id', $request->get('follow_by'))->first();

                if ($followTo && $followBy) {

                    $followUnfollow = FollowFollowing::where('followed_to', $request->get('follow_to'))->where('followed_by', $request->get('follow_by'))->first();
                    if ($followUnfollow) {

                        $delete = FollowFollowing::where('followed_to', $request->get('follow_to'))->where('followed_by', $request->get('follow_by'))->delete();
                        return \Response::json(['message'=>'Unfollow.', 'response_code' => 200], 200);
                    } else {

                        FollowFollowing::create([
                            'followed_to' => $request->get('follow_to'),
                            'followed_by' => $request->get('follow_by'),
                        ]);

                        return \Response::json(['message'=>'User started following.', 'response_code' => 200], 200);
                    }
                } else {
                    return \Response::json(['message'=>'User Id does not exists.', 'response_code' => 401], 401);
                }
            }
        }
        catch (Exception $e) {
            return Response::json(["line"=>$e->getLine(),"message"=>$e->getMessage()], 500);
        }  
    }


    /**
     * Function Name : userlogout
     * Description   : Logout Api
     *
     * @param  $user_id    user_id
     *
     * @return response
     */
    public function userlogout(Request $request)
    {
        try { 

            // Check parameter
            if (empty($request->get('user_id'))) {
                return \Response::json(['message' => 'User id is required.', 'response_code' => 400], 400);
            } else {

                // Check user if exists
                $exists = User::where('id', $request->get('user_id'))->first();

                if ($exists) {
                   

                    $delete = UserDevice::where('user_id', $request->get('user_id'))->where('device_token', $request->get('device_token'))->delete();

                    return \Response::json(['message'=>'User logged out successfully.', 'response_code' => 200], 200);
                } else {
                    return \Response::json(['message' => 'User does not exists.', 'response_code' => 401], 401);
                }
            }
        }
        catch (Exception $e) {
            return Response::json(["line" => $e->getLine(), "message" => $e->getMessage()], 500);
        }  
    }


    /**
     * Function Name : rateApp
     * Description   : Give Rate To App Api
     *
     * @param  $user_id    user_id
     * @param  $rating     rating
     *
     * @return response
     */
    public function rateApp(Request $request)
    {
        try { 

            // Check parameter
            if (empty($request->get('user_id'))) {
                return \Response::json(['message' => 'User id is required.', 'response_code' => 400], 400);
            } else {

                // Check user if exists
                $exists = User::where('id', $request->get('user_id'))->first();

                if ($exists) {

                    AppReview::create([
                        'user_id' => $request->get('user_id'),
                        'rating'  => $request->get('rating'),
                    ]);

                    return \Response::json(['message'=>'App reviewed successfully.', 'response_code' => 200], 200);
                } else {
                    return \Response::json(['message' => 'User does not exists.', 'response_code' => 401], 401);
                }
            }
        }
        catch (Exception $e) {
            return Response::json(["line" => $e->getLine(), "msg" => $e->getMessage()], 500);
        }  
    }


    /**
     * Function Name : staticContent
     * Description   : Static Content Api
     *
     * @param  $type    type
     *
     * @return response
     */
    public function staticContent(Request $request)
    {
        try { 

            $static = StaticContent::where('type', $request->get('type'))->first();

            if ($static) {

                $resp            = array();
                $resp['type']    = $static->type;
                $resp['name']    = $static->name;
                $resp['content'] = $static->content;

                return \Response::json(['data' => $resp, 'message'=>'Static content fetched successfully.', 'response_code' => 200], 200);
            } else {
                return \Response::json(['message'=>'No static content found.', 'response_code' => 401], 401);
            }
        }
        catch (Exception $e) {
            return Response::json(["line" => $e->getLine(), "message" => $e->getMessage()], 500);
        }  
    }




     /**
     * Function Name : addressVerfication
     * Description   : Address Verfication Api
     *
     * @param  $user_id           user_id
     * @param  $phone_number      phone_number
     * @param  $documents         documents
     * @param  $user_document     user_document
     *
     * @return response
     */
    public function addressVerfication(Request $request)
    {
        try { 

            // Check if parameter exists
            if (empty($request->user_id)) {
                return \Response::json(['message' => 'User id is required.', 'response_code' => 400], 400);
            } 

            // Load user from user id
            $user = User::where('id', $request->get('user_id'))->first();

            // Check if user exists
            if ($user) {

                //Check User Delete
                if($this->userDeleted($request->get('user_id'))){
                    return \Response::json(['message'=>'Your account has been deleted. Please contact to admin.', 'response_code' => 302], 302);
                }

                //Check User Block
                if($this->userBlocked($request->get('user_id'))){
                    return \Response::json(['message'=>'You are blocked by admin. Please contact to admin.', 'response_code' => 303], 303);
                }
                 $user_document = $request->user_document;
                 // Documents Check
                if (!empty($user_document)) {
                    //$image = $request->profile_image;
                    $documentUpload =  Storage::putFileAs('uploads/document_upload', $user_document, $user_document->getClientOriginalName());
                }

                //print_r($documentUpload);exit;

                if($request->type == 'otp'){
                    //generate 4 digit otp
                    $randomNumber = rand(1000,9999);                    
                    
                    if( isset($request->phone_number) && isset($request->country_code) && !empty($request->phone_number) && !empty($request->country_code)  )
                    {
                        $user->phone_number = $request->phone_number;
                        $user->country_code = $request->country_code;
                        $user->otp = $randomNumber;
                        $user->save();
                    }
                    else
                    {
                        $user->otp = $randomNumber;
                        $user->save();
                    }
                    
                    $message = 'Your OTP from AssetsCo is - '.$randomNumber;
                    $userPhoneNumber = $user->country_code.$user->phone_number;

                    $sendMsg = Helper::sendSms($userPhoneNumber, $message);
                    
                    return \Response::json(['data' => $randomNumber ,'message'=>'OTP send to registered mobile number successfully.', 'response_code' => 200], 200);
                   
                }elseif ($request->type == 'documents') {
                    //Check If phone number
                    if (empty($request->user_document)) {
                        return \Response::json(['message'=>'Document required.', 'response_code' => 302], 302);
                    }

                   $user->documents = ($documentUpload) ? $documentUpload : $user->documents;
                   $user->save();
                   return \Response::json(['message'=>'Document uploaded successfully.', 'response_code' => 200], 200);
                }else{
                    return \Response::json(['message' => 'Invalid type.', 'response_code' => 401], 401);
                }

                

            } else {

                return \Response::json(['message' => 'User does not exists.', 'response_code' => 401], 401);
            }
        } catch(Exception $e) {
            return Response::json(["line" => $e->getLine(), "message" => $e->getMessage()], 500);
        }
    }

    /**
     * Function Name : otpVerfication
     * Description   : OTP Verfication Api
     *
     * @param  $user_id           user_id
     * @param  $otp               otp
     *
     * @return response
     */
    public function otpVerfication(Request $request)
    {
        try { 

            // Check if parameter exists
            if (empty($request->user_id)) {
                return \Response::json(['message' => 'User id is required.', 'response_code' => 400], 400);
            } 

            // Load user from user id
            $user = User::where('id', $request->get('user_id'))->first();

            // Check if user exists
            if ($user) {

                //Check User Delete
                if($this->userDeleted($request->get('user_id'))){
                    return \Response::json(['message'=>'Your account has been deleted. Please contact to admin.', 'response_code' => 302], 302);
                }

                //Check User Block
                if($this->userBlocked($request->get('user_id'))){
                    return \Response::json(['message'=>'You are blocked by admin. Please contact to admin.', 'response_code' => 303], 303);
                }
                
                if($user->otp ==  $request->otp){
                    $user->address_verified = '2';
                    $user->otp = '';
                    $user->save();
                   return \Response::json(['message'=>'OTP verified successfully.', 'response_code' => 200], 200);
                }else{
                    return \Response::json(['message'=>'Please enter valid OTP.', 'response_code' => 404], 404);
                }

            } else {

                return \Response::json(['message' => 'User does not exists.', 'response_code' => 401], 401);
            }
        } catch(Exception $e) {
            return Response::json(["line" => $e->getLine(), "message" => $e->getMessage()], 500);
        }
    }

    /**
     * Function Name : notificationOnOff
     * Description   : OTP Verfication Api
     *
     * @param  $user_id           user_id
     * @param  $status            status 1 for notification on 0 for notification off
     *
     * @return response
     */
    public function notificationOnOff(Request $request)
    {
        try { 

            // Check if parameter exists
            if (empty($request->user_id)) {
                return \Response::json(['message' => 'User id is required.', 'response_code' => 400], 400);
            } 

            // Load user from user id
            $user = User::where('id', $request->get('user_id'))->first();

            // Check if user exists
            if ($user) {

                //Check User Delete
                if($this->userDeleted($request->get('user_id'))){
                    return \Response::json(['message'=>'Your account has been deleted. Please contact to admin.', 'response_code' => 302], 302);
                }

                //Check User Block
                if($this->userBlocked($request->get('user_id'))){
                    return \Response::json(['message'=>'You are blocked by admin. Please contact to admin.', 'response_code' => 303], 303);
                }
                //status 1 for notification on 0 for notification off
                if($request->status ==  '1'){
                    $user->notification_status = $user->status;
                    $user->save();
                   return \Response::json(['message'=>'Notification On successfully.', 'response_code' => 200], 200);
                }else{
                    $user->notification_status = 0;
                    $user->save();
                    return \Response::json(['message'=>'Notification Off successfully.', 'response_code' => 200], 200);
                }

            } else {

                return \Response::json(['message' => 'User does not exists.', 'response_code' => 401], 401);
            }
        } catch(Exception $e) {
            return Response::json(["line" => $e->getLine(), "message" => $e->getMessage()], 500);
        }
    }



     /**
     * Function Name : userBlocked
     * Description   : Check User Block or Not
     *
     * @param  $type    user_id
     *
     * @return response
     */
    public function userBlocked($user_id)
    {
        try { 

            $userBlock = User::where('id', $user_id)->first();
            //Check User blocked or not
            if($userBlock->is_blocked == 1){
            	return 1;
            }
           
        }
        catch (Exception $e) {
            return Response::json(["line" => $e->getLine(), "message" => $e->getMessage()], 500);
        }  
    }


     /**
     * Function Name : userDeleted
     * Description   : Check User Delete or Not
     *
     * @param  $type    user_id
     *
     * @return response
     */
    public function userDeleted($user_id)
    {
        try { 

            $userBlock = User::where('id', $user_id)->first();
            //print_r($userBlock);exit;
            //Check User deleted or not
            if($userBlock->is_deleted == 1){
            	return 1;
            }
           
        }
        catch (Exception $e) {
            return Response::json(["line" => $e->getLine(), "message" => $e->getMessage()], 500);
        }  
    }


}
