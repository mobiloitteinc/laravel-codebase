<?php
/**
 * NoticeBoardController File Document Comment
 *
 * PHP Version 7.1
 *
 * @category Controllers
 * @package  Controller
 * @author   Mobiloitte <it@mobiloitte.in>
 * @license  http://mobiloitte.com/ GNU General Public License
 * @link     app/Http/Controllers/Website
*/
namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Helper\Helper;
use Illuminate\Support\Facades\Storage;
use App\User;
use Exception;
use Response;
use Validator;
use Auth;

/**
 * NoticeBoardController Class Doc Comment
 *
 * @category Class
 * @package  Controller
 * @author   Mobiloitte <it@mobiloitte.in>
 * @license  http://mobiloitte.com/ GNU General Public License
 * @link     app/Http/Controllers/Website
*/

class NoticeBoardController extends Controller
{
    /**
     * Function Name : load
     * Description   : load notice board html view
     *
     * @return response
    */
    public function load()
    {
        return view('website.notice_board');
    }
}