<?php
/**
 * HomeController File Document Comment
 *
 * PHP Version 7.1
 *
 * @category Controllers
 * @package  Controller
 * @author   Mobiloitte <it@mobiloitte.in>
 * @license  http://mobiloitte.com/ GNU General Public License
 * @link     app/Http/Controllers
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

/**
 * HomeController Class Doc Comment
 *
 * @category Class
 * @package  Controller
 * @author   Mobiloitte <it@mobiloitte.in>
 * @license  http://mobiloitte.com/ GNU General Public License
 * @link     app/Http/Controllers
*/

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }


    /**
     * Logout from website
     *
     * @return \Illuminate\Http\Response
     */
     public function logout()
    {
        Auth::logout();
        return Redirect::route('login');
    }
    
    /**
     * Logout from admin
     *
     * @return \Illuminate\Http\Response
     */
    public function adminLogout()
    {
        Auth::logout();
        return Redirect::route('admin.login');
    }
}
