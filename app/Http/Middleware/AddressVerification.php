<?php

namespace App\Http\Middleware;

use Closure;
use App\UserRole;
use Redirect;

class AddressVerification
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->check())
        {
            $user_id = auth()->user()->id;
            
            $user = auth()->user();

            $userRole = UserRole::where('user_id', $user_id)->first();

            if($userRole->role_id == 2){
                switch ($user->address_verified) {
                    case 0:
                        return Redirect::route('confirm-address');
                    break;
                    case 1:
                        return Redirect::route('verification.type');
                    break;
                    case 2:
                        return $next($request);
                    break;    
                    default:
                        return $next($request);
                    break;
                }
            }else{
                 return Redirect::route('admin.dashboard');
            }
        }
        else
        {
           return $next($request);
        }
    }
}
