<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Notifications\Notification;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class Registration extends Mailable
{
    use Queueable, SerializesModels;

    protected $token;
    /**
     * Create a new message instance.
     *
     * Registration constructor.
     * @param $token
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return (new MailMessage) 
                $this->from('admin@assetco.com')->subject('Activation email')->greeting('Hello!')->line('You need to activate your email before you can start using all of our services.')->action('Activate Email', route('authenticated.activate', ['token' => $this->token]))->line('Thank you for using our application!');
               /* ->view('emails.email')->with([
                        'orderName' => $this->order->name,
                        'orderPrice' => $this->order->price,
                    ]);;*/
    }
}

                    
                    
                    
                    