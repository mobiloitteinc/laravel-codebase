<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyAttribute extends Model
{
    protected $table = 'property_attributes';

    //return $this->hasOne('App\Attribute', 'id');
   // return $this->belongsTo('App\Attribute');

    public function attributeName()
    {
    	return $this->belongsTo('App\Attribute', 'attribute_id', 'id');
    }

    
}
