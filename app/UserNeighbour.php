<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserNeighbour extends Model
{
    protected $table = 'users_neighbourhood';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'name', 'latitude', 'longitude', 'image',
    ];
}
