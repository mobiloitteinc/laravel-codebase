<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyFollow extends Model
{
   protected $table = 'property_follow';
}
