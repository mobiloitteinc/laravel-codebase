<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\UserRole;
use App\User;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$role = Role::count();
    	$user = User::count();

    	if($role == 0){
	    	//for role admin
	        $role = new Role;
	        $role->name = 'administrator';
	        $role->save();
			
			//for role lister or seller
			$role = new Role;
			$role->name = 'user';
			$role->save();
			echo 'roles table data inserted';
		}

		if($user == 0){
			//for user admin
			User::create(
	            [
	                'name' => 'assets co',
	                'email' => 'assets@demo.com',
	                'password' => Hash::make('admin@1234'),
	                'phone_number' => '9999933399',
	                'gender' => 'Male',
	                'class' => 'C',
	                'country_code'=>'+91'
	            ]
	        );

	       //for insertion of user roles table
	       UserRole::create(['user_id'=>1,'role_id'=>1]); 
		   echo 'user_roles table data inserted';
		}
    }
}
