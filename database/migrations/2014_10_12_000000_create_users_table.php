<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'users',
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('name', 255);
                $table->string('email', 100)->unique();
                $table->string('country_code', 10)->nullable();
                $table->string('phone_number', 16)->unique()->nullable();
                $table->string('class', 5)->default('C');
                $table->string('date_of_birth', 50)->nullable();
                $table->string('cover_image', 255)->nullable();
                $table->string('profile_image', 255)->nullable();
                $table->boolean('is_deleted')->comment('0 for not delete and 1 for delete user')->default(false);
                $table->boolean('is_blocked')->comment('0 for active and 1 for blocked')->default(false);
                $table->boolean('email_verified')->comment('0 for not verified and 1 for email verified')->default(false);
                $table->boolean('address_verified')->comment('0 for not verified and 1 for adress fill up and 2 for address verified')->default(false);
                $table->string('otp',11)->nullable();
                $table->string('social_id',255)->nullable();
                $table->string('social_type',255)->nullable();
                $table->string('longitude', 255)->nullable();
                $table->string('latitude', 255)->nullable();
                $table->string('apartment', 255)->nullable();
                $table->string('street_address', 255)->nullable();
                $table->string('zip_code', 255)->nullable();
                $table->string('documents', 255)->nullable();
                $table->string('gender', 10);
                $table->string('password')->nullable();
                $table->string('provider')->nullable();
                $table->string('provider_id')->nullable();
                $table->string('referral_code', 10)->nullable();
                $table->string('referred_code', 10)->nullable();
                $table->boolean('notification_status', 10)->comment('0 for notification Off  and 1 for notification On')->default(false);
                $table->rememberToken();
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
