@component('mail::message')
# Hello!

You are receiving this email because we received a activation request for your account.

@component('mail::button', ['url' => $inputs['url']])
Reset Password
@endcomponent

If you did not request a password reset, no further action is required.


Thanks,<br>
{{ config('app.name') }}
@endcomponent