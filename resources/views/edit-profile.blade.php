@extends('layouts.logged_in')
@section('content')
<main class="main-container">
   <div class="column-tow">
      <div class="container">
         <!-- globle_inside Start -->
         <div class="globle_inside">
            <!-- OuterBlockBox-->
            <!-- outerBlockBox Start -->
            <div class="outerBlockBox">
               @include('_partials.logged-in-sidebar')
               <!-- middleFeedBox Start -->
               <div class="middleFeedBox">
                  <!-- globalCard Start -->
                  <div class="globalCard">
                      <form class="commonForm" id="save-edit-profile" method="POST" action="{{ route('save-edit-profile') }}" enctype="multipart/form-data">
                        @csrf
                        <!-- formBox Start -->
                        <div class="formBox center-box max-WT-600">
                           <div class="profileImg center-box mb15 mt15">
                              @if(!empty(auth()->user()->profile_image))
                                <img class="radius100" src="{{ url('storage/'.auth()->user()->profile_image)  }}" alt="Image"/>
                              @else
                                <img class="radius100" src="{{ asset('img/no-image.png') }}" alt="Image"/>
                              @endif
                              <span class="camera"><i class="fas fa-camera"></i><input id="file_chng" type="file" name="file"></span>
                           </div>
                           <!-- Row Start -->
                           <div class="row">
                              <div class="col-md-12">
                                 <div class="form-group">
                                    <label class="control-label">Name</label>
                                     <input type="text" name="full_name" class="form-control custom-control" placeholder="Full Name*" required="" maxlength="90" minlength="2" value="{{ old('full_name',$user->name) }}" />
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label">Email</label>
                                    <input type="text" name="email" class="form-control custom-control" placeholder="Email*" maxlength="90" required="" value="{{ old('email',$user->email) }}">
                                    @if ($errors->has('email'))
                                      <div class="error">{{ $errors->first('email') }}</div>
                                    @endif
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label">Phone Number</label>
                                    <input type="text" name="phone_number" class="form-control custom-control" placeholder="Phone Number*" required="" minlength="6" maxlength="20" value="{{ old('phone_number',$user->phone_number) }}">
                                     @if ($errors->has('phone_number'))
                                      <div class="error">{{ $errors->first('phone_number') }}</div>
                                    @endif
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label">Gender</label>
                                    <div class="radioBox">
                                       <label class="radio-button mb0">
                                       @if(strtolower($user->gender) == 'male')
                                        <input type="radio" id="test1" name="gender" value="male" checked="" />
                                         <label for="test1"  class="mb0">Male</label>
                                       @else
                                        <input type="radio" id="test1" name="gender" value="male" />
                                         <label for="test1"  class="mb0">Male</label>
                                       @endif
                                       </label>
                                       <label class="radio-button mb0">
                                        @if(strtolower($user->gender) == 'female')
                                        <input type="radio" id="test2" name="gender" value="female" checked="" />
                                        <label for="test2" class="mb0">Female</label>
                                      @else
                                        <input type="radio" id="test2" name="gender" value="female" />
                                        <label for="test2" class="mb0">Female</label>
                                      @endif 
                                       </label>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label">DOB</label>
                                    <input id="dob" type="text" name="date_of_birth" class="form-control custom-control" value="{{ old('date_of_birth',$user->date_of_birth) }}" />
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label">Street Address</label>
                                    <input type="text" name="street_address" class="form-control custom-control" value="{{ old('street_address',$user->street_address) }}" required="">
                                 </div>
                                 <div class="form-group mb30">
                                    <label class="control-label">Apartment</label>
                                    <input type="text" name="apartment" class="form-control custom-control" required="" value="{{ old('apartment',$user->apartment) }}" />
                                 </div>
                              </div>
                           </div>
                           <!-- Row End -->
                         <div class="form-group text-center">
                             <button type="submit" class="btn btnYellow  max-WT-200 minHeight40 font14"><span>Update</span></button>
                           </div>
                        </div>
                        <!-- formBox End -->
                        
                     </form>
                  </div>
                  <!-- globalCard End -->            
               </div>
               <!-- middleFeedBox End -->
              <!-- sideBarFooter Start -->
              @include('_partials.logged-in-footer')
              <!-- sideBarFooter End -->
            </div>
            <!-- OuterBlockBox End -->
         </div>
         <!-- globle_inside End -->
      </div>
   </div>
</main>
@endsection

@section('scripts')
{!! Toastr::message() !!}
<script type="text/javascript">
  $("#save-edit-profile").validate({
    focusInvalid: false,
    ignore: [],
    rules: {
      full_name: {
          required: true,
          minlength:2,
          maxlength:90
      },
      email: {
          required:true,
          email:true,
          maxlength:90
      },
      phone_number:{
        required:true,
        minlength:6,
        maxlength:20,
        validphone:true
      },
    },
    messages: {
       full_name: {
        required: 'Filling full name is mandatory.',
        
      },
       email: {
        required: 'Filling email address is mandatory.',
        
      },
      phone_number:{
        required:  'Filling phone number is mandatory.'
      },
    },
  });

  jQuery.validator.addMethod('validphone', function(value, element, param) {
  return this.optional(element) || /^([0-9]|[(-+.)])+?$/.test(value);    
}, "Please enter a valid phone number.");
  
var maxDate = createDate(0,0,5);

function createDate(days, months, years) {
    var date = new Date(); 
    date.setDate(date.getDate() - days);
    date.setMonth(date.getMonth() - months);
    date.setFullYear(date.getFullYear() - years);
    return date;    
}


$("#dob").datepicker({
   format: 'dd-mm-yyyy',
   autoclose:true,
   endDate: maxDate
});
</script>
@endsection