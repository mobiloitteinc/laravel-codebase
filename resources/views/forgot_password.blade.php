@extends('layouts.app')
     @section('content')
	    <section class="common-section-main">
          <div class="common-container">
            <div class="row">
               <div class="col-12">
                  <div class="common-content-body">
                    <div class="form-main-heading">
                      <h3>FORGOT PASSWORD</h3>
                    </div>
                      @if(Session::has('message'))
                    <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-dismissible">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                       {{ Session::get('message') }}
                    </div>
                @endif
                    <div class="form-body">
                      <form id="reset" action="{{ route('send-password-link') }}" method="post">
                      @csrf
                        <p class="form-para">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </p>
                        <div class="row">
                          <div class="col-12 col-sm-6">
                            <div class=" form-group">
                              <input type="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} custom-control input-icon-email" placeholder="Email Address*" value="{{ old('email') }}" required autofocus>
                              @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                            </div>
                            <button type="submit" class="btn btnYellow w-100 minHeight40 font16"><span>Forgot Password</span></button>
                          </div>
                       
                        
                        </div>

                         

                      </form>
                    </div>
                  </div>
              </div>
            </div>
          </div>
      </section>
     

       <!-- forgot Password Modal -->
      <div class="modal fade common-modal" id="forgot-pass" role="dialog">
        <div class="modal-dialog">
        
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">FORGOT PASSWORD</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              
            </div>
            <div class="modal-body change-pass-body">
              <p class="modal-para">Please enter the registered email address so that we can send you a password reset instructions.</p>

              <div class=" form-group">
                <input type="text" name="" class="form-control custom-control input-icon-email" placeholder="Email Address*">
              </div>
              <div class="mt40">
                <button class="btn btnYellow w-100 minHeight40 font16">Submit</button>
              </div>
              <p class="new-user resend">
                <a href="#">Resend Password?</a>
              </p>
            </div>
           
          </div>
          
        </div>
      </div>
      <!--      -->

      @endsection

      @section('scripts')
      <script type="text/javascript">
        $('#reset').validate({
          focusInvalid: false,
          ignore: [],
          rules: {
              email: {
                required: true,
                 remote: {
                  url : "{{ route('valid-email')}}",
                  type : "GET",
                  dataFilter: function (data) {
                          var json = JSON.parse(data);
                          if (json)
                              return 'false';
                          else 
                              return 'true';
                      },    
                  },
              },
            password: {
              required: true
            }
          },
          messages: {
            email: {
              required: 'Please enter email address.',
              remote:  'Email address does not exists.'
            },
          },
          submitHandler: function(form) {
            form.submit();
          },
          errorPlacement: function(error, element) {
              if (element.attr("name") == "email" || element.attr("name") == "password" )
                  $('.invalid-feedback').remove();
                  error.insertAfter(element);
          }
        });
      </script>
      @endsection