@extends('layouts.app')
     @section('content')
<!DOCTYPE html>
<html lang="en">

   <body>


    <main class="main-container">
      <section class="Payment-section common-section-main">
           <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="common-content-body mb20">
                    <div class="row-content-box">
                       <div class="pannel-header-s">
                          <h3>Settings</h3>
                       </div>
                       <div class="setting-inner">
                          <ul>
                             <li class="clearfix">
                                <div class="text-left fleft">
                                   <h4>Notifications</h4>
                                </div>
                                <div class="clearfix">
                                  <div class="toogle-box fright">
                                     <button type="button" class="btn btn-lg btn-toggle active" data-toggle="button" aria-pressed="true" autocomplete="off">
                                       <div class="handle"></div>
                                     </button>
                                  </div>  
                                </div>    
                             </li>
                              <li class="clearfix">
                                <a href="rate.html">
                                  <div class="text-left fleft">
                                     <h4>Rate App</h4>
                                  </div>
                                  <div class="clearfix">
                                    <div class="arrow-btn fright">
                                      <img src="img/blue-right-arrow.png" alt="arrow">
                                    </div>  
                                  </div>
                                </a>    
                             </li>
                             <li class="clearfix">
                                <a href="rate.html">
                                  <div class="text-left fleft">
                                     <h4>Invite Friends</h4>
                                  </div>
                                  <div class="clearfix">
                                    <div class="arrow-btn fright">
                                      <img src="img/blue-right-arrow.png" alt="arrow">
                                    </div>  
                                  </div>
                                </a>    
                             </li>
                             <li class="clearfix">
                                <a href="rate.html">
                                  <div class="text-left fleft">
                                     <h4>Terms & Conditions</h4>
                                  </div>
                                  <div class="clearfix">
                                    <div class="arrow-btn fright">
                                      <img src="img/blue-right-arrow.png" alt="arrow">
                                    </div>  
                                  </div>
                                </a>    
                             </li>
                             <li class="clearfix">
                                <a href="{{ route('edit-profile') }}">
                                  <div class="text-left fleft">
                                     <h4>Edit Profile</h4>
                                  </div>
                                  <div class="clearfix">
                                    <div class="arrow-btn fright">
                                      <img src="img/blue-right-arrow.png" alt="arrow">
                                    </div>  
                                  </div>
                                </a>    
                             </li>
                             <li class="clearfix">
                                <a href="{{ route('logout') }}">
                                  <div class="text-left fleft">
                                     <h4>Logout</h4>
                                  </div>
                                  <div class="clearfix">
                                    <div class="arrow-btn fright">
                                      <img src="img/blue-right-arrow.png" alt="arrow">
                                    </div>  
                                  </div>
                                </a>    
                             </li>
                          </ul>
                       </div>
                    </div>
                  </div>
              </div>
            </div>
          </div>
      </section>
    </main>
    
   
    
   </body>
</html>
</html>
      @endsection