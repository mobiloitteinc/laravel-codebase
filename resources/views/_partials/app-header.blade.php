<header class="inner-page-header">
   <div class="header-inner">
      <div class="logo">
        <a href="{{ url('/') }}">
          <img src="{{ asset('img/logo.png') }}" alt="Logo"/>
         </a>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="col">
               <span class="btn btn-mobmenu"><i class="fa fa-bars"></i></span>
               <div class="top-left-menu menu">
                  <ul>
                     @if(auth()->check())
                        <li><a href="javascript:void(0);">What we do</a></li>
                        <li><a href="javascript:void(0);">How it works</a></li>
                        <li><a href="javascript:void(0);">Locations</a></li>
                        <li><a href="javascript:void(0);">Guides</a></li>
                    @endif
                  </ul>
               </div>
               <div class="top-right-menu">
                   @if(!auth()->check())
                      <a class="btn btn-blue"  href="{{ route('login') }}">Sign In / Join</a>
                      <a href="javascript:;" class="btn btn-download btn-small blue-overlay"><span>Download the App</span></a>
                    @elseif( auth()->user()->address_verified == 2 )
                      <a class="btn btn-blue"  href="{{ route('settings') }}">Settings</a>   
                      <a class="btn btn-blue"  href="{{ route('logout') }}">Logout</a>
                    @else
                      <a class="btn btn-blue"  href="{{ route('logout') }}">Logout</a>
                   @endif
               </div>
            </div>
         </div>
      </div>
   </div>
</header>