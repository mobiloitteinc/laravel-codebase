@extends('layouts.app')

@section('content')
<main class="main-container">
 
  <section class="common-section-main">
      <div class="common-container">
         @if (session('status'))
          <div class="alert alert-success  alert-dismissible">
             <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          {{ session('status') }}
          </div>
          @endif
          @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                </div>
            @endif
        <div class="row">
           <div class="col-12">
              <div class="common-content-body">
                <div class="form-main-heading">
                  <h3>EDIT PROFILE</h3>
                </div>
                <div class="form-body">
                  <form id="save-edit-profile" method="POST" action="{{ route('save-edit-profile') }}" enctype="multipart/form-data">
                    @csrf
                          <!--  <img src="images/user-profile.png" alt="Logo" class="profile-pic" /> -->
                    <div class="pro-upload-block">
                       <div class="profile-pic">
                        @if(!empty($user->profile_image))
                          <img id="img_upload" src="{{ url('storage/'.$user->profile_image)  }}" class="user-image" width="130" height="130">
                        @else
                          <img id="img_upload" src="img/no-image.png" class="user-image" width="130" height="130">
                        @endif  
                            <span>
                              <input id="file_chng" type="file" name="file">
                              <img src="img/camera.png">
                            </span>
                       
                       </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <input type="text" name="full_name" class="form-control custom-control" placeholder="Full Name*" required="" maxlength="90" minlength="2" value="{{ old('full_name',$user->name) }}" />
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class=" form-group">
                          <input type="text" name="email" class="form-control custom-control" placeholder="Email*" maxlength="90" required="" value="{{ old('email',$user->email) }}">
                        </div>
                      </div>
                      
                      <div class="col-sm-6">
                        <div class=" form-group">
                          <input type="text" name="phone_number" class="form-control custom-control" placeholder="Phone Number*" required="" minlength="6" maxlength="20" value="{{ old('full_name',$user->phone_number) }}">
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class=" form-group">
                          <div class="radio-block">
                            <div class="row">
                                <label class="col-3">Gender</label>
                                <div class="col-9">
                                  <p class="radio-button">
                                    @if(strtolower($user->gender) == 'male')
                                      <input type="radio" id="test1" name="gender" value="male" checked="" />
                                    @else
                                      <input type="radio" id="test1" name="gender" value="male" />
                                    @endif
                                    <label for="test1">Male</label>
                                  </p>
                                  <p class="radio-button">
                                    @if(strtolower($user->gender) == 'female')
                                      <input type="radio" id="test2" name="gender" value="female" checked="" />
                                    @else
                                      <input type="radio" id="test2" name="gender" value="female" />
                                    @endif  
                                    <label for="test2">Female</label>
                                  </p>
                                </div>
                              </div>
                            </div>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class=" form-group">
                          <input type="text" name="street_address" class="form-control custom-control" placeholder="Street Address" value="{{ old('street_address',$user->street_address) }}">
                        </div>
                      </div>
                      <!-- <div class="col-sm-12">
                        <div class=" form-group">
                          <input type="text" name="referral_code" class="form-control custom-control" placeholder="Referral Code (Optional)">
                        </div>
                      </div> -->
                     
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                          <button class="btn btn-blue w-100 btn-common" type="submit">Update</button>
                        </div>
                        <div class="col-sm-6">
                          <button class="btn btn-blue btn-grey w-100 btn-common" data-toggle="modal" data-target="#change-pass" type="button">Change Password</button>
                        </div>
                    </div>

                  </form>
                </div>
              </div>
          </div>
        </div>
      </div>
  </section>
</main>
       <!-- Change Password Modal -->
      <div class="modal fade common-modal" id="change-pass" role="dialog">
        <div class="modal-dialog">
        
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">CHANGE PASSWORD</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              
            </div>

             <form id="change-password" action="{{ route('update-password') }}" method="post">
                      @csrf
            <div class="modal-body change-pass-body">
              <div class="form-group">
                <input type="password" name="old_password" class="form-control custom-control" placeholder="Old Password*">
              </div>
              <div class="form-group">
                <input type="password" name="password" id="password" class="form-control custom-control" placeholder="New Password*">
              </div>
              <div class="form-group">
                <input type="password" name="password_confirmation" class="form-control custom-control" placeholder="Confirm New Password*">
              </div>
              <div class="form-group mt30">
                <input type="submit" name="" value="Change Password" class="btn btn-blue w-100 btn-common"></input>
              </div>
              <div class="">
                <button id="closemodal" class="btn btn-blue btn-grey w-100 btn-common">Cancel</button>
              </div>
            </div>
          </form>
           
          </div>
          
        </div>
      </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $("#save-edit-profile").validate({
    focusInvalid: false,
    ignore: [],
    rules: {
      full_name: {
          required: true,
          minlength:2,
          maxlength:90
      },
      email: {
          required:true,
          email:true,
          maxlength:90
      },
      phone_number:{
        required:true,
        minlength:6,
        maxlength:20,
        validphone:true
      },
    },
    messages: {
       full_name: {
        required: 'Filling full name is mandatory.',
        
      },
       email: {
        required: 'Filling email address is mandatory.',
        
      },
      phone_number:{
        required:  'Filling phone number is mandatory.'
      },
    },
  });

  $('#change-password').validate({
    focusInvalid: false,
    ignore: [],
    rules: {
         old_password:{
            required:true,
            minlength:8,
            maxlength:16,
            remote: {
            url : "{{ route('validate-password')}}",
            type : "GET",
            dataFilter: function (data) {
                    var json = JSON.parse(data);
                    console.log(json)
                    if (json)
                        return 'true';
                    else 
                        return 'false';
                },    
            },
        },
      password: {
            required: true,
            minlength:8,
            maxlength:16
        },
        password_confirmation: {
            required:true,
            equalTo: "#password"
        },
    },


    messages: {
  
       password: {
        required: 'Please enter new password.',
        
      },
       password_confirmation: {
        required: 'Please enter confirm password.',
        
      },
      old_password:{
                required: 'Please enter old password.',
                remote:  'Old password does not match.'
        }
    },

    submitHandler: function(form) {
      form.submit();
    }
  });

  //profile
  function readURL(input) {

    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('#img_upload').attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#file_chng").change(function() {
    readURL(this);
  });

  jQuery.validator.addMethod('validphone', function(value, element, param) {
    return this.optional(element) || /^([0-9]|[(-+.)])+?$/.test(value);    
  }, "Please enter a valid phone number.");


  

</script>
@endsection