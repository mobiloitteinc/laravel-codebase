@extends('layouts.admin')

@section('content')
        <main class="middle-content">
            <!-- Page Title Start -->
            <div class="page_title_block">
                <h1 class="page_title">View User</h1>
               
            </div>
            <!-- Page Title End -->
            <div class="content-section">

                <div class="order-view mt30 max-WT-700 mrgn-0-auto">
                    <div class="main-block-innner mb40 mt40">
                        <div class="add-store-block input-style">
                            <div class="form-group row align-items-top">
                                <label class="col-md-5">Profile Image :</label>
                                <div class="col-md-7">
                                    @if($user->profile_image)
                                        <img class="img-circle image image-profile-user" src="{{ asset('storage/'.$user->profile_image) }}">
                                    @else
                                        <img class="img-circle image image-profile-user" src="{{ asset('storage/uploads/profile_images/default_image.png') }}">
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row align-items-baseline">
                                <label class="col-md-5">Name :</label>
                                <div class="col-md-7">
                                    <label>{{ $user->name }}</label>
                                </div>
                            </div>
                            <div class="form-group row align-items-baseline">
                                <label class="col-md-5">Email :</label>
                                <div class="col-md-7">
                                    <label> {{ $user->email }} </label>
                                </div>
                            </div>
                            <div class="text-left mt40">
                                <a href="{{ route('user.edit.profile') }}" class="btn btn-info">Edit Profile</a>
                             </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
       
@endsection