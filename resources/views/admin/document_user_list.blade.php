@extends('layouts.admin')

@section('content')
        <main class="middle-content">
            <!-- Page Title Start -->
            <div class="page_title_block">
                <h1 class="page_title">Document Management</h1>
                <div class="common-bredcrumb">
                    <ol class="breadcrumb">

                        <li class="breadcrumb-item active">Document Management</li>
                    </ol>

                </div>
            </div>
            <!-- Page Title End -->
            <div class="content-section">
                <div class="outer-box">
                   <!--  <div class="table-search p0 mb10">
                        <div class="filter_search mb20">
                            <div class="input-group filter_search_group">
                                <input type="text" class="form-control" placeholder="Search">
                                <div class="input-group-append">
                                    <button class="btn btn_search_group" type="button"><img src="{{ url('admin/img/icon-search.png') }}" alt="Search"></button>
                                </div>
                            </div>
                        </div>
                       

                    </div> -->
                    <!-- Gloabl Table Box Start -->
                    <div class="global-table no-radius p0">

                        <div class="tab-content">                          
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr class="no_wrap_th">
                                                <th>Name</th>
                                                <th>Email ID</th>
                                                <th>Mobile Number</th>
                                                <th class="action_td_btn3">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($users as $value)
                                            <tr>
                                              
                                                <td>{{ $value->name }}</td>
                                                <td>{{ $value->email }}</td>
                                                <td>{{ $value->country_code.$value->phone_number }}</td>

                                                <td class="action_td_btn3">
                                                    <a href="{{ url('admin/upload/user/list/'.$value->id) }}"  class="btn btn-info btn-raised">View Document</a>
                                                </td>
                                            </tr>
                                            @empty
                                                <tr class="text-center">
                                                    <td colspan="4">
                                                        {{ 'No Data Found!' }}
                                                    </td>
                                                </tr>
                                            @endforelse

                                        </tbody>
                                    </table>
                                </div>
                                <div class="custom-pagination mt20 text-center">
                                  {{ $users->links() }}
                                </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- Table Responsive End -->
    </div>
    </main>
   
    </div>
   
@endsection