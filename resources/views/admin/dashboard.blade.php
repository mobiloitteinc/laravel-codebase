@extends('layouts.admin')

@section('content')

    <main class="middle-content">
        <!-- Page Title Start -->
        <div class="page_title_block">
            <h1 class="page_title">Dashboard</h1>
           <!--  <div class="common-bredcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>

            </div> -->
        </div>
        <!-- Page Title End -->
        <div class="content-section">
            <ul class="dash_list d-flex w-100 flex-wrap text-center justify-content-between">
              
                <li>
                    <div class="dash_icon">
                        <i class="fas fa-user"></i>
                    </div>
                      <a href="{{ route('admin.user.list') }}">
                    <h4>Total Users</h4>
                    <h3>{{ $usercount }}</h3>
                      </a>
                </li>
          
                <li>
                    <div class="dash_icon">
                        <i class="fa fa-home"></i>
                    </div>
                     <a href="{{ route('admin.property.list') }}">
                    <h4>No. Property</h4>
                    <h3>{{ $propertyCount }}</h3>
                    </a>
                </li>
              <li>
                    <div class="dash_icon">
                        <i class="fa fa-file"></i>
                    </div>
                     <a href="{{ route('admin.upload.user.list') }}">
                    <h4>Document Mangemanet</h4>
                    <h3>{{ $addresscount }}</h3>
                    </a>
                </li>
                <!--  <li>
                    <div class="dash_icon">
                        <i class="fas fa-thermometer-quarter"></i>
                    </div>
                    <h4>Indoor Temp.</h4>
                    <h3>25C</h3>
                </li>
                <li>
                    <div class="dash_icon">
                        <i class="fas fa-thermometer-three-quarters"></i>
                    </div>
                    <h4>Outdoor Temp.</h4>
                    <h3>30C</h3>
                </li>
                <li>
                    <div class="dash_icon">
                        <i class="fas fa-expand"></i>
                    </div>
                    <h4>Indoor Humidity</h4>
                    <h3>56%</h3>
                </li>
                <li>
                    <div class="dash_icon">
                        <i class="fas fa-stopwatch"></i>
                    </div>
                    <h4>Alarms Major</h4>
                    <h3>37 Sec</h3>
                </li>
                <li>
                    <div class="dash_icon">
                        <i class="fas fa-clock"></i>
                    </div>
                    <h4>Alarms Minor</h4>
                    <h3>48 Sec</h3>
                </li> -->
            </ul>


        </div>
    </main>
</div>
<!-- Wrapper End -->
<!--Modal Start-->
<div class="modal fade global-modal reset-modal" id="delet_farms_modal">
    <div class="modal-dialog max-WT-500">
        <div class="modal-content">
            <div class="inner_border_area">
                <!-- Modal body -->
                <div class="modal-body  text-center">
                    <div class="row align-items-center modal_flax_height">
                        <div class="col">
                            <div class="box-title mb40 d-inline-block">
                                <h2>Are you sure?</h2>
                                <p>You won’t be able to revert this!</p>
                            </div>
                            <div class="max-WT-300 d-inline-block">
                                <button type="button" class="btn btn-gray btn-large radius0 btn-block">YES</button>
                                <button type="button" class="btn btn-red btn-large radius0 btn-block" data-dismiss="modal">CANCEL</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- signout_modal Start -->
<div class="modal fade global-modal reset-modal" id="signout_modal">
    <div class="modal-dialog max-WT-500">
        <div class="modal-content">
            <div class="inner_border_area">
                <!-- Modal body -->
                <div class="modal-body  text-center">
                    <div class="row align-items-center modal_flax_height">
                        <div class="col">
                            <div class="box-title mb40 d-inline-block">
                                <i class="fas fa-power-off off-icon"></i>
                                <p class="mt40">Are you sure you want to logout?</p>
                            </div>
                            <div class="text-center">
                                <a href="login.html" class="btn btn-blue btn-noYes">YES</a>
                                <button type="button" class="btn btn-red btn-noYes" data-dismiss="modal">CANCEL</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Signout Modal -->
<!-- Change Password_modal Start -->
<div class="modal fade global-modal reset-modal" id="change_passwprd_modal">
    <div class="modal-dialog max-WT-500">
        <form class="change_password">
            <div class="modal-content">
                <div class="inner_border_area">
                    <div class="modal-header ">
                        <h4 class="modal-title text-center">Change Password</h4>
                        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                    </div>
                    <div class="modal-body">
                        <div class="row align-items-center modal_flax_height">
                            <div class="col">
                                <div class="form-group">
                                    <label class="control-labe">Old Password</label>
                                    <input class="form-control" placeholder="" required="" type="text">
                                </div>
                                <div class="form-group">
                                    <label class="control-labe">New Password</label>
                                    <input class="form-control" placeholder="" required="" type="text">
                                </div>
                                <div class="form-group">
                                    <label class="control-labe">Confirm Password</label>
                                    <input class="form-control" placeholder="" required="" type="text">
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <button type="submit" class="btn btn-gray btn-large radius0 btn-block">SUBMIT</button>
                                    </div>
                                    <div class="col-6">
                                        <button type="button" class="btn btn-red btn-large radius0 btn-block" data-dismiss="modal">CANCEL</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- Change Password_modal End -->

<!-- /.content -->
@endsection