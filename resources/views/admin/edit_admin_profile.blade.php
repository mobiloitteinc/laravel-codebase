@extends('layouts.admin')

@section('content')
        <!-- Aside End -->
        <main class="middle-content">
            <!-- Page Title Start -->
            <div class="page_title_block">
                <h1 class="page_title">Edit Admin</h1>
             
            </div>
            <!-- Page Title End -->
            <div class="content-section">

                <div class="order-view mt30 max-WT-700 mrgn-0-auto">
                    <div class="main-block-innner mb40 mt40">
                        <form id="admin-edit"  method="POST" action="{{ route('user.update.profile') }}"  enctype="multipart/form-data">
                            @csrf
                        <div class="add-store-block input-style">
                             <div class="form-group row align-items-center">
                                <label class="col-md-4">Name :</label>
                                <div class="col-md-8">
                                @if(!empty($user->profile_image))
                                     <img id="img_upload" class="img-circle image image-profile-user" src="{{ url('storage/'.$user->profile_image)  }}" class="user-image" >
                                @else
                                    <img id="img_upload" class="img-circle image image-profile-user" src="img/no-image.png" class="user-image">
                                @endif  
                            <span>
                              <input id="file_chng" type="file" name="file">
                            </span>
                                </div>
                            </div>
                            <div class="form-group row align-items-center">
                                <label class="col-md-4">Name :</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control form-control2" name="full_name" value="{{ $user->name }}" />
                                </div>
                            </div>
                        
                            <div class="form-group row align-items-center">
                                <label class="col-md-4">Email :</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control form-control2" name="email" value="{{ $user->email }}" />
                                </div>
                            </div>
                            <div class="text-center mt40">
                                <button type="submit"  class="btn btn-large  max-WT-200 font-100 btn-green" >Save</button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </main>
      
    </div>
@endsection
@section('scripts')
<script type="text/javascript">
  $("#admin-edit").validate({
    focusInvalid: false,
    ignore: [],
    rules: {
      full_name: {
          required: true,
          minlength:2,
          maxlength:50
      },
      email: {
          required:true,
          email:true,
          maxlength:90
      }
    
    },
    messages: {
       full_name: {
        required: 'Filling full name is mandatory.',
        
      },
       email: {
        required: 'Filling email address is mandatory.',
        
      }
      
    },
  });

  $('#change-password').validate({
    focusInvalid: false,
    ignore: [],
    rules: {
         old_password:{
            required:true,
            minlength:8,
            maxlength:16,
            remote: {
            url : "{{ route('validate-password')}}",
            type : "GET",
            dataFilter: function (data) {
                    var json = JSON.parse(data);
                    console.log(json)
                    if (json)
                        return 'true';
                    else 
                        return 'false';
                },    
            },
        },
      password: {
            required: true,
            minlength:8,
            maxlength:16
        },
        password_confirmation: {
            required:true,
            equalTo: "#password"
        },
    },


    messages: {
  
       password: {
        required: 'Please enter new password.',
        
      },
       password_confirmation: {
        required: 'Please enter confirm password.',
        
      },
      old_password:{
                required: 'Please enter old password.',
                remote:  'Old password does not match.'
        }
    },

    submitHandler: function(form) {
      form.submit();
    }
  });





  

</script>
@endsection



