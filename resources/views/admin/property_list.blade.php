@extends('layouts.admin')

@section('content')
        <main class="middle-content">
            <!-- Page Title Start -->
            <div class="page_title_block">
                <h1 class="page_title">Property Management</h1>
              
            </div>
            <!-- Page Title End -->
            <div class="content-section">
                <div class="outer-box">
                    <!-- Gloabl Table Box Start -->
                    <div class="global-table no-radius p0">

                        <div class="tab-content">                          
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr class="no_wrap_th">
                                                <th  width="20%">Property Name</th>
                                                <th  width="10%">Property Type</th>
                                                <th>Property Price</th>
                                                <th>Property Created Date</th>
                                                <th>Property Status</th>
                                                <th class="action_td_btn3">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($property as $value)
                                            <tr>
                                              
                                                <td  width="20%">{{ $value->property_name }}</td>
                                                @if($value->property_type == 1)
                                                <td  width="10%">Apartment</td>
                                                @elseif($value->property_type == 2)
                                                <td  width="20%">House</td>
                                                @else
                                                <td  width="20%">Room</td>
                                                @endif
                                                <td>{{ $value->price_for_property }}</td>
                                                <td>{{ date('m-d-Y',strtotime($value->created_at)) }}</td>
                                                 @if($value->is_blocked == 1)
                                                <td>Blocked</td>
                                                @else
                                                <td>Active</td>
                                                @endif

                                                <td class="action_td_btn3">
                                                    <a href="{{ route('admin.property.view', $value->id) }}" class="btn btn-info btn-raised">View</a>
                                                    <button type="button" data-toggle="modal" data-target="#delete" class="btn btn-danger btn-raised">Delete</button>
                                                    <button type="button" data-toggle="modal" data-target="#block" class="btn btn-raised btn-warning">Block</button>
                                                </td>
                                            </tr>
                                            @empty
                                                <tr class="row">
                                                    <td>
                                                        {{ 'No Data Found!' }}
                                                    </td>
                                                </tr>
                                            @endforelse

                                        </tbody>
                                    </table>
                                </div>
                                <div class="custom-pagination mt20 text-center">
                                  {{ $property->links() }}
                                </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- Table Responsive End -->
    </div>
    </main>
   
    </div>   
  
@endsection