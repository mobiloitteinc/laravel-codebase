@extends('layouts.admin')

@section('content')
        <main class="middle-content">
            <!-- Page Title Start -->
            <div class="page_title_block">
                <h1 class="page_title">View User</h1>
               
            </div>
            <!-- Page Title End -->
            <div class="content-section">

                <div class="order-view mt30 max-WT-700 mrgn-0-auto">
                    <div class="main-block-innner mb40 mt40">
                        <div class="add-store-block input-style">
                            <div class="form-group row align-items-baseline">
                                <label class="col-md-5">Profile Image :</label>
                                <div class="col-md-7">
                                    @if($user->profile_image)
                                        <img class="img-circle image image-profile-user" src="{{ asset('storage/'.$user->profile_image) }}">
                                    @else
                                        <img class="img-circle image image-profile-user" src="{{ asset('storage/uploads/profile_images/default_image.png') }}">
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row align-items-baseline">
                                <label class="col-md-5">Name :</label>
                                <div class="col-md-7">
                                    <label>{{ $user->name }}</label>
                                </div>
                            </div>
                            <div class="form-group row align-items-baseline">
                                <label class="col-md-5">Email :</label>
                                <div class="col-md-7">
                                    <label> {{ $user->email }} </label>
                                </div>
                            </div>
                            <div class="form-group row align-items-baseline">
                                <label class="col-md-5">Mobile Number :</label>
                                <div class="col-md-7">
                                    <label>{{ $user->country_code.$user->phone_number }}</label>
                                </div>
                            </div>
                             @if($user->address_verified == 2)
                            <div class="form-group row align-items-baseline">
                                <label class="col-md-5">Email Auth. :</label>
                                <div class="col-md-7">
                                    <label class="badge badge-success">VERIFIED</label>
                                </div>
                            </div>
                            @else
                             <div class="form-group row align-items-baseline">
                                <label class="col-md-5">Email Auth. :</label>
                                <div class="col-md-7">
                                    <label class="badge badge-danger">NOT VERIFIED</label>
                                </div>
                            </div>
                            @endif
                            @if($user->email_verified == 1)
                            <div class="form-group row align-items-baseline">
                                <label class="col-md-5">Address Auth. :</label>
                                <div class="col-md-7">
                                    <label class="badge badge-success">VERIFIED</label>
                                </div>
                            </div>
                            @else
                             <div class="form-group row align-items-baseline">
                                <label class="col-md-5">Address Auth. :</label>
                                <div class="col-md-7">
                                    <label class="badge badge-danger">NOT VERIFIED</label>
                                </div>
                            </div>
                            @endif

                            <div class="form-group row align-items-baseline">
                                <label class="col-md-5">Date of Birth :</label>
                                <div class="col-md-7">
                                    @if($user->date_of_birth)
                                        <label>{{ date('m-d-Y',$user->date_of_birth) }}</label>
                                    @else
                                    <label>-</label>
                                    @endif
                                </div>
                            </div>
                              <div class="form-group row align-items-baseline">
                                <label class="col-md-5">Address :</label>
                                <div class="col-md-7">
                                    <label>{{ $user->street_address }}</label>
                                </div>
                            </div>
                              <div class="form-group row align-items-baseline">
                                <label class="col-md-5">Apartment :</label>
                                <div class="col-md-7">
                                    <label>{{ $user->apartment }}</label>
                                </div>
                            </div>
                              <div class="form-group row align-items-baseline">
                                <label class="col-md-5">Zip Code :</label>
                                <div class="col-md-7">
                                    <label>{{ $user->zip_code }}</label>
                                </div>
                            </div>
                               <div class="form-group row align-items-baseline">
                                <label class="col-md-5">Gender :</label>
                                <div class="col-md-7">
                                    <label>{{ $user->gender }}</label>
                                </div>
                            </div>
                          
                     
                              
                            <div class="text-left mt40">
                             </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
       
@endsection