@extends('layouts.app')
@section('content')
<section class="common-section-main">
  <div class="common-container">
    <div class="row">
       <div class="col-12">
          <div class="common-content-body">
            <div class="form-main-heading">
              <h3>OTP VERIFICATION</h3>
            </div>
            @if (session('message'))
            <div class="alert {{ session('alert-class') }}  alert-dismissible">
               <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session('message') }}
            </div>
            @endif
              @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                </div>
            @endif
            <div class="form-body">
             <form method="post" action="{{ route('verification.otp') }}">
                @csrf  
                <p class="form-para">An OTP has been sent to your registered mobile number.</p>
                <div class="otp-block">
                   <div class="row">
                      <div class="col-sm-12">
                        <div class=" form-group">
                         <p class="form-para">Please enter the OTP</p>
                        </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="form-group">
                       <div class="autotabbed d-flex justify-content-between">
                            <input class="txtboxToFilter" type="text" name="pin[]" maxlength="1" size="1" required="" value="" />
                            <input class="txtboxToFilter" type="text" name="pin[]" maxlength="1" size="1" required="" value=""/>
                            <input class="txtboxToFilter" type="text" name="pin[]" maxlength="1" size="1" required="" value=""/>
                            <input class="txtboxToFilter" type="text" name="pin[]" maxlength="1" size="1" required="" value=""/>
                        </div>
                    </div>
                    </div>
                   <div class="col-sm-12 text-center">
                         <button type="submit" class="btn btnYellow max-WT-200 minHeight40 font16"><span>Submit</span></button>
                      </div>
                   </div>
                </div>   
              </form>
            </div>
          </div>
      </div>
    </div>
  </div>
</section>
@endsection
@section('scripts')
<script type="text/javascript">
 $(".txtboxToFilter").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
         // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});
 //move to next cursor
$(".txtboxToFilter").keyup(function() {
    if (this.value.length == 1) {
      $(this).next('.txtboxToFilter').focus();
    }
});

</script>
@endsection