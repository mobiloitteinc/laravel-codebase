@extends('layouts.app')

@section('stylesheet')
  <link rel="stylesheet" href="{{ asset('js/web-scripts/intl-tel-input-13.0.0/build/css/intlTelInput.css') }}"/>
  <style type="text/css">
    .iti-flag {background-image: url(" {{ asset('js/web-scripts/intl-tel-input-13.0.0/build/img/flags.png') }}");}

    @media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min--moz-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2 / 1), only screen and (min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {
      .iti-flag {background-image: url(" {{ asset('js/web-scripts/intl-tel-input-13.0.0/build/img/flags@2x.png') }}");}
    }
    .intl-tel-input {width: 100%;}
  </style>
@endsection

@section('content')
<section class="common-section-main">
  <div class="common-container">
    <div class="row">
       <div class="col-12">
          <div class="common-content-body">
            <div class="form-main-heading">
              <h3>MOBILE NUMBER UPDATION</h3>
            </div>
            @if (session('message'))
            <div class="alert {{ session('alert-class') }}  alert-dismissible">
               <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session('message') }}
            </div>
            @endif
              @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                </div>
            @endif
            <div class="form-body">
             <form id="mobile_number_save" method="post" action="{{ route('mobile-number-social') }}">
                @csrf  
                <p class="form-para">Enter your mobile number</p>
                <div class="otp-block">
                   <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                              <input type="tel" id="phone" class="form-control txtboxToFilter" name="phone_number" value="{{ old('phone_number') }}">
                              <input id="calling_code" type="hidden" name="country_code" value="">
                              @if ($errors->has('phone_number'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </span>
                              @elseif ($errors->has('country_code'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('country_code') }}</strong>
                                    </span>
                              @endif
                          </div>
                    </div>
                    <div class="col-sm-12 text-center">
                      <button type="submit" class="btn btn-blue  btn-common max-WT-150">Submit</button>
                    </div>
                   </div>
                </div>   
              </form>
            </div>
          </div>
      </div>
    </div>
  </div>
</section>
@endsection
@section('scripts')
<script type="text/javascript" src="{{ asset('js/web-scripts/intl-tel-input-13.0.0/build/js/intlTelInput.js') }}"></script>
<script type="text/javascript">
  function initCountryPhone( phone_input_id){
      // get the country data from the plugin

        var telInput = $('#'+phone_input_id);

        // init plugin
        telInput.intlTelInput({
          utilsScript: "{{ asset('js/web-scripts/intl-tel-input-13.0.0/build/js/utils.js') }}" // just for formatting/placeholders etc
          ,customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
              return "Mobile number*";
            }
        });
        
      }

  initCountryPhone("phone", "");

 $(".txtboxToFilter").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
         // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});

$('#mobile_number_save').validate({
  errorElement: 'span',
  focusInvalid: false,
  ignore: [],
  rules: {
      phone_number:{
          required:true,
          digits:true,
          minlength:6,
          maxlength:20
      },
  },
  messages: {
      phone_number:{
          required:'Filling mobile number is mandatory.',
          digits:"Please enter a valid mobile number."           
      },
  },
  submitHandler: function(form) {
    $("#calling_code").val($("#phone").intlTelInput("getSelectedCountryData").dialCode);   
    form.submit();
  }
});
</script>
@endsection