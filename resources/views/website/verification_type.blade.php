@extends('layouts.app')
@section('content')
<section class="common-section-main">
    <div class="common-container">
        <div class="row">
            <div class="col-12">
                <div class="common-content-body">
                    <div class="form-main-heading">
                        <h3>ADDRESS VERIFICATION</h3>
                    </div>
                    <div class="form-body address-verfiy-body">
                       <form method="post" id="confirm_address" action="{{ route('check-for-verification') }}">   
                            @csrf
                            @if(!empty(auth()->user()->documents))
                                <p class="form-para alert alert-warning">Welcome! Your documents has been uploaded. Please give us time to review it or you can use phone number for instant address verification</p>
                            @else
                                <p class="form-para">Welcome! Please confirm your address to ensure privacy, address verification  is required.</p>
                            @endif
                            <p class="notaddress text-center mt20">
                                 <a href="{{ route('confirm-address') }}">Not your address? Click here to update your address</a>
                              </p>
                            <div class="row">
                              <div class="col-sm-12">
                                <div class=" form-group text-center">
                                  <p class="radio-button">
                                        <input type="radio" id="test1" name="verify" value="phone">
                                        <label for="test1">Phone instant and free</label>
                                      </p>
                                </div>
                              </div>
                              <div class="col-sm-12">
                                <div class=" form-group text-center">
                                    <p class="radio-button">
                                        <input id="test2" type="radio" name="verify" value="document">
                                        <label for="test2">Document verification</label>
                                      </p>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                  <button class="btn btn-blue w-100 btn-common" type="submit">Next</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection