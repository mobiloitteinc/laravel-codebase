@extends('layouts.logged_in')
@section('content')
<main class="main-container">
 <div class="column-tow">
    <div class="container">
       <!-- globle_inside Start -->
       <div class="globle_inside">
          <!-- OuterBlockBox-->
          <!-- outerBlockBox Start -->
          <div class="outerBlockBox">
              @include('_partials.logged-in-sidebar')
             <!-- middleFeedBox Start -->
             <div class="middleFeedBox">
      
       <!-- topPostCommentBlock Start-->
      <div class="topPostCommentBlock">
     <form action="notice-board.html">
    <!-- composeCommentBox Start -->
       <div class="composeCommentBox composeCommentNotExpand">
         <div class="composeHead">
         <span class="composeBtn compCommon active"><img src="{{ asset('img/edit-icon.png') }}" alt="Icon"/>Compose Post</span>
         <span class="albumBtn compCommon">
           <img src="{{ asset('img/photo-album.png') }}" alt="Icon"/>Photo Album<input class="fileCommon" type="file"/>
         </span>
         <button type="button" class="closeCompose"><img src="{{ asset('img/cross-out.png') }}" alt="Icon"/></button>
       </div>
       <div class="composeBody">
         <div class="composeArea">
           <span class="feedUserImg"><img src="{{ asset('img/write-img.png') }}" alt="image"></span>
         <div class="composeAreaField">
           <textarea  rows='1' class="form-control" placeholder="Write something here"></textarea>
           <span class="addIcons"><button type="button" class="btnEmoji"><i class="fas fa-smile"></i></button></span>
         </div>
         </div>
         
         <div class="composeActivity">
           <span class="activityBtn">
           <img src="{{ asset('img/photo.png') }}" alt="image">Photo<input class="fileCommon" type="file"/>
         </span>
         </div>
         
         <!-- showOnExpand Start -->
         <div class="showOnExpand">
           <div class="text-center">
           <button type="button" class="btn btnYellow w-100">
           <span>Post</span>
           </button>
        </div>
         </div>
         <!-- showOnExpand End -->
       </div>
       </div>
       <!-- composeCommentBox End -->
       </form>
    </div>
   <!-- topPostCommentBlock End-->
   
                <!-- feedCardBox Start -->
                <div class="feedCardBox">
                   <!-- feedUserDetailBox Start -->
                   <div class="feedUserDetailBox">
                      <span class="feedUserImg"><img src="{{ asset('img/write-img.png') }}" alt="image"/></span>
                      <div class="feedUserDetail">
                         <h5>Rohit Sharma</h5>
                         <label>Mar 6 at 11:00 am</label>
                      </div>
                      <button class="btn btn-follow">Follow</button>
                   </div>
                   <!-- feedUserDetailBox End -->
                   <!-- feedSliderWithInfo Start -->
                   <div class="feedSliderWithInfoBox">
                      <ul class="feedStrip">
                         <li><i class="fas fa-bed"></i>2</li>
                         <li><i class="fas fa-bath"></i>2</li>
                         <li><i class="fas fa-car"></i>2</li>
                      </ul>
                      <div class="feedSlider">
                         <div class="feedItem">
                            <figure><img src="{{ asset('img/fav_fig.png') }}" alt="Fig"/></figure>
                         </div>
                         <div class="feedItem">
                            <figure><img src="{{ asset('img/fav_fig.png') }}" alt="Fig"/></figure>
                         </div>
                         <div class="feedItem">
                            <figure><img src="{{ asset('img/fav_fig.png') }}" alt="Fig"/></figure>
                         </div>
                         <div class="feedItem">
                            <figure><img src="{{ asset('img/fav_fig.png') }}" alt="Fig"/></figure>
                         </div>
                      </div>
                      <div class="feedAddressInfo">
                         <div class="feedAddressRate">
                            <p>House No-235, Okhala Delhi, 400120</p>
                            <label><strong>Rate: $400</strong></label>
                         </div>
                         <div class="feedCompLogo">
                            <img src="{{ asset('img/logo.png') }}" alt="Logo"/>
                         </div>
                      </div>
                   </div>
                   <!-- feedSliderWithInfo End -->
                   <!-- feedActionBox Start -->
                   <div class="feedActionBox">
                      <ul class="feedActionList">
                         <li><a class="likeTap"href="javascript:void(0);"><i class="far fa-thumbs-up"></i>Like</a></li>
                         <li><a class="commentTap" href="javascript:void(0);"><i class="far fa-comment-alt"></i>Comment</a></li>
                         <li><a class="shareTap" href="javascript:void(0);"><i class="fas fa-share"></i>Share</a></li>
                      </ul>
                      <div class="feedCommentBox">
                         <span class="feedUserImg"><img src="{{ asset('img/write-img.png') }}" alt="image"></span>
                         <div class="commentField">
                            <input type="text" class="form-control"/>
                            <div class="commentSmily">
                               <button type="button" class="btnCamera">
               <i class="fas fa-camera"></i>
             <input type="file"/>
           </button>
                               <button type="button" class="btnEmoji"><i class="fas fa-smile"></i></button>
                            </div>
                         </div>
                      </div>
        
        <!-- commentBoxOuter Start -->
        <div class="commentBoxOuter">
          <!-- Post Comment Box Start -->
           <div class="postCommentBox">
           <span class="feedUserImg"><img src="{{ asset('img/write-img.png') }}" alt="image"></span>
           <div class="postCommentDetail">
             <p><a class="postUserName" href="profile.html">David</a>Lorem ipsum dummy text</p>
           <div class="reComment">
             <button class="reLikeBtn">Like</button>
             <button class="reReplyBtn">Reply</button>
             <span class="reCommtTime">Mar 6 at 11:00 am</span>
           </div>
           </div>
         </div>
         <!-- Post Comment Box End -->
         
         <!-- Post Comment Box Start -->
           <div class="postCommentBox">
           <span class="feedUserImg"><img src="{{ asset('img/notif-img2.png') }}" alt="image"></span>
           <div class="postCommentDetail">
             <p><a class="postUserName" href="profile.html">Milan</a>Lorem ipsum dummy text</p>
           <div class="reComment">
             <button class="reLikeBtn">Like</button>
             <button class="reReplyBtn">Reply</button>
             <span class="reCommtTime">Mar 6 at 11:00 am</span>
           </div>
           
            <!-- Post Inner Comment Box Start -->
             <div class="postCommentBox">
               <span class="feedUserImg"><img src="{{ asset('img/profile-img.png') }}" alt="image"></span>
               <div class="postCommentDetail">
               <p><a class="postUserName" href="profile.html">Rohit</a>Lorem ipsum dummy text</p>
               <div class="reComment">
                 <button class="reLikeBtn">Like</button>
                 <button class="reReplyBtn">Reply</button>
                 <span class="reCommtTime">Mar 6 at 11:00 am</span>
               </div>
               </div>
             </div>
             <!-- Post Inner Comment Box End -->
           </div>
         </div>
         <!-- Post Comment Box End -->
         <!-- Post Comment Box Start -->
           <div class="postCommentBox hide">
           <span class="feedUserImg"><img src="{{ asset('img/write-img.png') }}" alt="image"></span>
           <div class="postCommentDetail">
             <p><a class="postUserName" href="profile.html">David</a>Lorem ipsum dummy text</p>
           <div class="reComment">
             <button class="reLikeBtn">Like</button>
             <button class="reReplyBtn">Reply</button>
             <span class="reCommtTime">Mar 6 at 11:00 am</span>
           </div>
           </div>
         </div>
         <!-- Post Comment Box End -->
         
        </div>
        <!-- commentBoxOuter End -->
        
        
        
        
                   </div>
                   <!-- feedActionBox End -->
                </div>
                <!-- feedCardBox End -->
                <!-- feedCardBox Start -->
                <div class="feedCardBox">
                   <!-- feedUserDetailBox Start -->
                   <div class="feedUserDetailBox">
                      <span class="feedUserImg"><img src="{{ asset('img/write-img.png') }}" alt="image"/></span>
                      <div class="feedUserDetail">
                         <h5>Rohit Sharma</h5>
                         <label>Mar 6 at 11:00 am</label>
                      </div>
                      <button class="btn btn-follow">Follow</button>
                   </div>
                   <!-- feedUserDetailBox End -->
                   <!-- feedSliderWithInfo Start -->
                   <div class="feedSliderWithInfoBox">
                      <ul class="feedStrip">
                         <li><i class="fas fa-bed"></i>2</li>
                         <li><i class="fas fa-bath"></i>2</li>
                         <li><i class="fas fa-car"></i>2</li>
                      </ul>
                      <div class="feedSlider">
                         <div class="feedItem">
                            <figure><img src="{{ asset('img/fav_fig.png') }}" alt="Fig"/></figure>
                         </div>
                         <div class="feedItem">
                            <figure><img src="{{ asset('img/fav_fig.png') }}" alt="Fig"/></figure>
                         </div>
                         <div class="feedItem">
                            <figure><img src="{{ asset('img/fav_fig.png') }}" alt="Fig"/></figure>
                         </div>
                         <div class="feedItem">
                            <figure><img src="{{ asset('img/fav_fig.png') }}" alt="Fig"/></figure>
                         </div>
                      </div>
                      <div class="feedAddressInfo">
                         <div class="feedAddressRate">
                            <p>House No-235, Okhala Delhi, 400120</p>
                            <label><strong>Rate: $400</strong></label>
                         </div>
                         <div class="feedCompLogo">
                            <img src="{{ asset('img/logo.png') }}" alt="Logo"/>
                         </div>
                      </div>
                   </div>
                   <!-- feedSliderWithInfo End -->
                   <!-- feedActionBox Start -->
                   <div class="feedActionBox">
                      <ul class="feedActionList">
                         <li><a class="likeTap"href="javascript:void(0);"><i class="far fa-thumbs-up"></i>Like</a></li>
                         <li><a class="commentTap" href="javascript:void(0);"><i class="far fa-comment-alt"></i>Comment</a></li>
                         <li><a class="shareTap" href="javascript:void(0);"><i class="fas fa-share"></i>Share</a></li>
                      </ul>
                      <div class="feedCommentBox">
                         <span class="feedUserImg"><img src="{{ asset('img/write-img.png') }}" alt="image"></span>
                         <div class="commentField">
                            <input type="text" class="form-control"/>
                            <div class="commentSmily">
                               <a href="javascript:void(0);"><i class="fas  fa-camera "></i></a>
                               <a href="javascript:void(0);"><i class="fas fa-smile"></i></a>
                            </div>
                         </div>
                      </div>
                   </div>
                   <!-- feedActionBox End -->
                </div>
                <!-- feedCardBox End -->
                                <!-- feedCardBox Start -->
                <div class="feedCardBox">
                   <!-- feedUserDetailBox Start -->
                   <div class="feedUserDetailBox">
                      <span class="feedUserImg"><img src="{{ asset('img/write-img.png') }}" alt="image"/></span>
                      <div class="feedUserDetail">
                         <h5>Rohit Sharma</h5>
                         <label>Mar 6 at 11:00 am</label>
                      </div>
                      <button class="btn btn-follow">Follow</button>
                   </div>
                   <!-- feedUserDetailBox End -->
                   <!-- feedSliderWithInfo Start -->
                   <div class="feedSliderWithInfoBox">
                      <ul class="feedStrip">
                         <li><i class="fas fa-bed"></i>2</li>
                         <li><i class="fas fa-bath"></i>2</li>
                         <li><i class="fas fa-car"></i>2</li>
                      </ul>
                      <div class="feedSlider">
                         <div class="feedItem">
                            <figure><img src="{{ asset('img/fav_fig.png') }}" alt="Fig"/></figure>
                         </div>
                         <div class="feedItem">
                            <figure><img src="{{ asset('img/fav_fig.png') }}" alt="Fig"/></figure>
                         </div>
                         <div class="feedItem">
                            <figure><img src="{{ asset('img/fav_fig.png') }}" alt="Fig"/></figure>
                         </div>
                         <div class="feedItem">
                            <figure><img src="{{ asset('img/fav_fig.png') }}" alt="Fig"/></figure>
                         </div>
                      </div>
                      <div class="feedAddressInfo">
                         <div class="feedAddressRate">
                            <p>House No-235, Okhala Delhi, 400120</p>
                            <label><strong>Rate: $400</strong></label>
                         </div>
                         <div class="feedCompLogo">
                            <img src="{{ asset('img/logo.png') }}" alt="Logo"/>
                         </div>
                      </div>
                   </div>
                   <!-- feedSliderWithInfo End -->
                   <!-- feedActionBox Start -->
                   <div class="feedActionBox">
                      <ul class="feedActionList">
                         <li><a class="likeTap"href="javascript:void(0);"><i class="far fa-thumbs-up"></i>Like</a></li>
                         <li><a class="commentTap" href="javascript:void(0);"><i class="far fa-comment-alt"></i>Comment</a></li>
                         <li><a class="shareTap" href="javascript:void(0);"><i class="fas fa-share"></i>Share</a></li>
                      </ul>
                      <div class="feedCommentBox">
                         <span class="feedUserImg"><img src="{{ asset('img/write-img.png') }}" alt="image"></span>
                         <div class="commentField">
                            <input type="text" class="form-control"/>
                            <div class="commentSmily">
                               <a href="javascript:void(0);"><i class="fas  fa-camera "></i></a>
                               <a href="javascript:void(0);"><i class="fas fa-smile"></i></a>
                            </div>
                         </div>
                      </div>
                   </div>
                   <!-- feedActionBox End -->
                </div>
                <!-- feedCardBox End -->
                               <!-- feedCardBox Start -->
                <div class="feedCardBox">
                   <!-- feedUserDetailBox Start -->
                   <div class="feedUserDetailBox">
                      <span class="feedUserImg"><img src="{{ asset('img/write-img.png') }}" alt="image"/></span>
                      <div class="feedUserDetail">
                         <h5>Rohit Sharma</h5>
                         <label>Mar 6 at 11:00 am</label>
                      </div>
                      <button class="btn btn-follow">Follow</button>
                   </div>
                   <!-- feedUserDetailBox End -->
                   <!-- feedSliderWithInfo Start -->
                   <div class="feedSliderWithInfoBox">
                      <ul class="feedStrip">
                         <li><i class="fas fa-bed"></i>2</li>
                         <li><i class="fas fa-bath"></i>2</li>
                         <li><i class="fas fa-car"></i>2</li>
                      </ul>
                      <div class="feedSlider">
                         <div class="feedItem">
                            <figure><img src="{{ asset('img/fav_fig.png') }}" alt="Fig"/></figure>
                         </div>
                         <div class="feedItem">
                            <figure><img src="{{ asset('img/fav_fig.png') }}" alt="Fig"/></figure>
                         </div>
                         <div class="feedItem">
                            <figure><img src="{{ asset('img/fav_fig.png') }}" alt="Fig"/></figure>
                         </div>
                         <div class="feedItem">
                            <figure><img src="{{ asset('img/fav_fig.png') }}" alt="Fig"/></figure>
                         </div>
                      </div>
                      <div class="feedAddressInfo">
                         <div class="feedAddressRate">
                            <p>House No-235, Okhala Delhi, 400120</p>
                            <label><strong>Rate: $400</strong></label>
                         </div>
                         <div class="feedCompLogo">
                            <img src="{{ asset('img/logo.png') }}" alt="Logo"/>
                         </div>
                      </div>
                   </div>
                   <!-- feedSliderWithInfo End -->
                   <!-- feedActionBox Start -->
                   <div class="feedActionBox">
                      <ul class="feedActionList">
                         <li><a class="likeTap"href="javascript:void(0);"><i class="far fa-thumbs-up"></i>Like</a></li>
                         <li><a class="commentTap" href="javascript:void(0);"><i class="far fa-comment-alt"></i>Comment</a></li>
                         <li><a class="shareTap" href="javascript:void(0);"><i class="fas fa-share"></i>Share</a></li>
                      </ul>
                      <div class="feedCommentBox">
                         <span class="feedUserImg"><img src="{{ asset('img/write-img.png') }}" alt="image"></span>
                         <div class="commentField">
                            <input type="text" class="form-control"/>
                            <div class="commentSmily">
                               <a href="javascript:void(0);"><i class="fas  fa-camera "></i></a>
                               <a href="javascript:void(0);"><i class="fas fa-smile"></i></a>
                            </div>
                         </div>
                      </div>
                   </div>
                   <!-- feedActionBox End -->
                </div>
                <!-- feedCardBox End -->
                                <!-- feedCardBox Start -->
                <div class="feedCardBox">
                   <!-- feedUserDetailBox Start -->
                   <div class="feedUserDetailBox">
                      <span class="feedUserImg"><img src="{{ asset('img/write-img.png') }}" alt="image"/></span>
                      <div class="feedUserDetail">
                         <h5>Rohit Sharma</h5>
                         <label>Mar 6 at 11:00 am</label>
                      </div>
                      <button class="btn btn-follow">Follow</button>
                   </div>
                   <!-- feedUserDetailBox End -->
                   <!-- feedSliderWithInfo Start -->
                   <div class="feedSliderWithInfoBox">
                      <ul class="feedStrip">
                         <li><i class="fas fa-bed"></i>2</li>
                         <li><i class="fas fa-bath"></i>2</li>
                         <li><i class="fas fa-car"></i>2</li>
                      </ul>
                      <div class="feedSlider">
                         <div class="feedItem">
                            <figure><img src="{{ asset('img/fav_fig.png') }}" alt="Fig"/></figure>
                         </div>
                         <div class="feedItem">
                            <figure><img src="{{ asset('img/fav_fig.png') }}" alt="Fig"/></figure>
                         </div>
                         <div class="feedItem">
                            <figure><img src="{{ asset('img/fav_fig.png') }}" alt="Fig"/></figure>
                         </div>
                         <div class="feedItem">
                            <figure><img src="{{ asset('img/fav_fig.png') }}" alt="Fig"/></figure>
                         </div>
                      </div>
                      <div class="feedAddressInfo">
                         <div class="feedAddressRate">
                            <p>House No-235, Okhala Delhi, 400120</p>
                            <label><strong>Rate: $400</strong></label>
                         </div>
                         <div class="feedCompLogo">
                            <img src="{{ asset('img/logo.png') }}" alt="Logo"/>
                         </div>
                      </div>
                   </div>
                   <!-- feedSliderWithInfo End -->
                   <!-- feedActionBox Start -->
                   <div class="feedActionBox">
                      <ul class="feedActionList">
                         <li><a class="likeTap"href="javascript:void(0);"><i class="far fa-thumbs-up"></i>Like</a></li>
                         <li><a class="commentTap" href="javascript:void(0);"><i class="far fa-comment-alt"></i>Comment</a></li>
                         <li><a class="shareTap" href="javascript:void(0);"><i class="fas fa-share"></i>Share</a></li>
                      </ul>
                      <div class="feedCommentBox">
                         <span class="feedUserImg"><img src="{{ asset('img/write-img.png') }}" alt="image"></span>
                         <div class="commentField">
                            <input type="text" class="form-control"/>
                            <div class="commentSmily">
                               <a href="javascript:void(0);"><i class="fas  fa-camera "></i></a>
                               <a href="javascript:void(0);"><i class="fas fa-smile"></i></a>
                            </div>
                         </div>
                      </div>
                   </div>
                   <!-- feedActionBox End -->
                </div>
                <!-- feedCardBox End -->
                                <!-- feedCardBox Start -->
                <div class="feedCardBox">
                   <!-- feedUserDetailBox Start -->
                   <div class="feedUserDetailBox">
                      <span class="feedUserImg"><img src="{{ asset('img/write-img.png') }}" alt="image"/></span>
                      <div class="feedUserDetail">
                         <h5>Rohit Sharma</h5>
                         <label>Mar 6 at 11:00 am</label>
                      </div>
                      <button class="btn btn-follow">Follow</button>
                   </div>
                   <!-- feedUserDetailBox End -->
                   <!-- feedSliderWithInfo Start -->
                   <div class="feedSliderWithInfoBox">
                      <ul class="feedStrip">
                         <li><i class="fas fa-bed"></i>2</li>
                         <li><i class="fas fa-bath"></i>2</li>
                         <li><i class="fas fa-car"></i>2</li>
                      </ul>
                      <div class="feedSlider">
                         <div class="feedItem">
                            <figure><img src="{{ asset('img/fav_fig.png') }}" alt="Fig"/></figure>
                         </div>
                         <div class="feedItem">
                            <figure><img src="{{ asset('img/fav_fig.png') }}" alt="Fig"/></figure>
                         </div>
                         <div class="feedItem">
                            <figure><img src="{{ asset('img/fav_fig.png') }}" alt="Fig"/></figure>
                         </div>
                         <div class="feedItem">
                            <figure><img src="{{ asset('img/fav_fig.png') }}" alt="Fig"/></figure>
                         </div>
                      </div>
                      <div class="feedAddressInfo">
                         <div class="feedAddressRate">
                            <p>House No-235, Okhala Delhi, 400120</p>
                            <label><strong>Rate: $400</strong></label>
                         </div>
                         <div class="feedCompLogo">
                            <img src="{{ asset('img/logo.png') }}" alt="Logo"/>
                         </div>
                      </div>
                   </div>
                   <!-- feedSliderWithInfo End -->
                   <!-- feedActionBox Start -->
                   <div class="feedActionBox">
                      <ul class="feedActionList">
                         <li><a class="likeTap"href="javascript:void(0);"><i class="far fa-thumbs-up"></i>Like</a></li>
                         <li><a class="commentTap" href="javascript:void(0);"><i class="far fa-comment-alt"></i>Comment</a></li>
                         <li><a class="shareTap" href="javascript:void(0);"><i class="fas fa-share"></i>Share</a></li>
                      </ul>
                      <div class="feedCommentBox">
                         <span class="feedUserImg"><img src="{{ asset('img/write-img.png') }}" alt="image"></span>
                         <div class="commentField">
                            <input type="text" class="form-control"/>
                            <div class="commentSmily">
                               <a href="javascript:void(0);"><i class="fas  fa-camera "></i></a>
                               <a href="javascript:void(0);"><i class="fas fa-smile"></i></a>
                            </div>
                         </div>
                      </div>
                   </div>
                   <!-- feedActionBox End -->
                </div>
                <!-- feedCardBox End -->
                                <!-- feedCardBox Start -->
                <div class="feedCardBox">
                   <!-- feedUserDetailBox Start -->
                   <div class="feedUserDetailBox">
                      <span class="feedUserImg"><img src="{{ asset('img/write-img.png') }}" alt="image"/></span>
                      <div class="feedUserDetail">
                         <h5>Rohit Sharma</h5>
                         <label>Mar 6 at 11:00 am</label>
                      </div>
                      <button class="btn btn-follow">Follow</button>
                   </div>
                   <!-- feedUserDetailBox End -->
                   <!-- feedSliderWithInfo Start -->
                   <div class="feedSliderWithInfoBox">
                      <ul class="feedStrip">
                         <li><i class="fas fa-bed"></i>2</li>
                         <li><i class="fas fa-bath"></i>2</li>
                         <li><i class="fas fa-car"></i>2</li>
                      </ul>
                      <div class="feedSlider">
                         <div class="feedItem">
                            <figure><img src="{{ asset('img/fav_fig.png') }}" alt="Fig"/></figure>
                         </div>
                         <div class="feedItem">
                            <figure><img src="{{ asset('img/fav_fig.png') }}" alt="Fig"/></figure>
                         </div>
                         <div class="feedItem">
                            <figure><img src="{{ asset('img/fav_fig.png') }}" alt="Fig"/></figure>
                         </div>
                         <div class="feedItem">
                            <figure><img src="{{ asset('img/fav_fig.png') }}" alt="Fig"/></figure>
                         </div>
                      </div>
                      <div class="feedAddressInfo">
                         <div class="feedAddressRate">
                            <p>House No-235, Okhala Delhi, 400120</p>
                            <label><strong>Rate: $400</strong></label>
                         </div>
                         <div class="feedCompLogo">
                            <img src="{{ asset('img/logo.png') }}" alt="Logo"/>
                         </div>
                      </div>
                   </div>
                   <!-- feedSliderWithInfo End -->
                   <!-- feedActionBox Start -->
                   <div class="feedActionBox">
                      <ul class="feedActionList">
                         <li><a class="likeTap"href="javascript:void(0);"><i class="far fa-thumbs-up"></i>Like</a></li>
                         <li><a class="commentTap" href="javascript:void(0);"><i class="far fa-comment-alt"></i>Comment</a></li>
                         <li><a class="shareTap" href="javascript:void(0);"><i class="fas fa-share"></i>Share</a></li>
                      </ul>
                      <div class="feedCommentBox">
                         <span class="feedUserImg"><img src="{{ asset('img/write-img.png') }}" alt="image"></span>
                         <div class="commentField">
                            <input type="text" class="form-control"/>
                            <div class="commentSmily">
                               <a href="javascript:void(0);"><i class="fas  fa-camera "></i></a>
                               <a href="javascript:void(0);"><i class="fas fa-smile"></i></a>
                            </div>
                         </div>
                      </div>
                   </div>
                   <!-- feedActionBox End -->
                </div>
                <!-- feedCardBox End -->
                                <!-- feedCardBox Start -->
                <div class="feedCardBox">
                   <!-- feedUserDetailBox Start -->
                   <div class="feedUserDetailBox">
                      <span class="feedUserImg"><img src="{{ asset('img/write-img.png') }}" alt="image"/></span>
                      <div class="feedUserDetail">
                         <h5>Rohit Sharma</h5>
                         <label>Mar 6 at 11:00 am</label>
                      </div>
                      <button class="btn btn-follow">Follow</button>
                   </div>
                   <!-- feedUserDetailBox End -->
                   <!-- feedSliderWithInfo Start -->
                   <div class="feedSliderWithInfoBox">
                      <ul class="feedStrip">
                         <li><i class="fas fa-bed"></i>2</li>
                         <li><i class="fas fa-bath"></i>2</li>
                         <li><i class="fas fa-car"></i>2</li>
                      </ul>
                      <div class="feedSlider">
                         <div class="feedItem">
                            <figure><img src="{{ asset('img/fav_fig.png') }}" alt="Fig"/></figure>
                         </div>
                         <div class="feedItem">
                            <figure><img src="{{ asset('img/fav_fig.png') }}" alt="Fig"/></figure>
                         </div>
                         <div class="feedItem">
                            <figure><img src="{{ asset('img/fav_fig.png') }}" alt="Fig"/></figure>
                         </div>
                         <div class="feedItem">
                            <figure><img src="{{ asset('img/fav_fig.png') }}" alt="Fig"/></figure>
                         </div>
                      </div>
                      <div class="feedAddressInfo">
                         <div class="feedAddressRate">
                            <p>House No-235, Okhala Delhi, 400120</p>
                            <label><strong>Rate: $400</strong></label>
                         </div>
                         <div class="feedCompLogo">
                            <img src="{{ asset('img/logo.png') }}" alt="Logo"/>
                         </div>
                      </div>
                   </div>
                   <!-- feedSliderWithInfo End -->
                   <!-- feedActionBox Start -->
                   <div class="feedActionBox">
                      <ul class="feedActionList">
                         <li><a class="likeTap"href="javascript:void(0);"><i class="far fa-thumbs-up"></i>Like</a></li>
                         <li><a class="commentTap" href="javascript:void(0);"><i class="far fa-comment-alt"></i>Comment</a></li>
                         <li><a class="shareTap" href="javascript:void(0);"><i class="fas fa-share"></i>Share</a></li>
                      </ul>
                      <div class="feedCommentBox">
                         <span class="feedUserImg"><img src="{{ asset('img/write-img.png') }}" alt="image"></span>
                         <div class="commentField">
                            <input type="text" class="form-control"/>
                            <div class="commentSmily">
                               <a href="javascript:void(0);"><i class="fas  fa-camera "></i></a>
                               <a href="javascript:void(0);"><i class="fas fa-smile"></i></a>
                            </div>
                         </div>
                      </div>
                   </div>
                   <!-- feedActionBox End -->
                </div>
                <!-- feedCardBox End -->
             </div>
             <!-- middleFeedBox End -->
             <!-- RightContentBox Start-->
            @include('_partials.logged-in-footer')
             <!-- RightContentBox End-->
          </div>
          <!-- OuterBlockBox End -->
       </div>
       <!-- globle_inside End -->
    </div>
 </div>
</main>
<!-- Main End -->
@endsection