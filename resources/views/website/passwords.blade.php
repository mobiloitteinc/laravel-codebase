@extends('layouts.app')
     @section('content')
            <section class="common-section-main">
          <div class="common-container">
            <div class="row">
               <div class="col-12">
                  <div class="common-content-body">
                    <div class="form-main-heading">
                      <h3>FORGOT PASSWORD</h3>
                    </div>
                      @if(Session::has('message'))
                        <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-dismissible">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                           {{ Session::get('message') }}
                        </div>
                    @endif
                    <div class="form-body">
                      <form id="reset" action="{{ url('reset-password').'/'.request()->token }}" method="post">
                      @csrf
                        <p class="form-para">A link will be sent to your registered email adress after submission of form.</p>
                        <div class="row">
                          <div class="col-12 col-sm-6">
                            <div class=" form-group">
                                 <input id="email" type="email" name="email" class="form-control input-icon-email" placeholder="Email Id" maxlength="60" required="" value="{{ old('email') }}" autofocus="" autocomplete="off" />
                              @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                            </div>
                            <div class=" form-group">
                              <input id="password" type="password" name="password" class="form-control" placeholder="New Password" minlength="8" maxlength="16" required="" />
                              @if ($errors->has('password'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                            </div>
                            <div class=" form-group">
                               <input type="password" name="confirm_password" class="form-control" placeholder="Confirm Password" minlength="8" maxlength="16" required="" />
                              @if ($errors->has('confirm_password'))
                                 <span class="error">{{ $errors->first('confirm_password') }}</span>
                               @endif
                            </div>                            

                           
                              <input type="submit" class="btn btn-blue w-100 btn-common" value="Submit">
                         
                          </div>
                       
                        
                        </div>

                         

                      </form>
                    </div>
                  </div>
              </div>
            </div>
          </div>
      </section>
@endsection


@section('scripts')
<script type="text/javascript">
$('#reset').validate({
  focusInvalid: false,
  ignore: [],
  rules: {
      email: {
        required: true,
         remote: {
          url : "{{ route('valid-email')}}",
          type : "GET",
          dataFilter: function (data) {
                  var json = JSON.parse(data);
                  if (json)
                      return 'false';
                  else 
                      return 'true';
              },    
          },
      },
    password: {
      required: true
    },
    confirm_password:{
      required:true,
      equalTo:"#password"
    }
  },
  messages: {
    email: {
      required: 'Please enter email address.',
      remote:  'Email address does not exists.'
    },
    password: {
      required: 'Please enter password.'
    },
    confirm_password: {
      required: 'Please enter password confirmation.',
      equalTo:'Password and confirm password must be same.'
    },
  },
  submitHandler: function(form) {
    form.submit();
  }
});
</script>
@endsection
