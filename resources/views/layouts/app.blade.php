<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0,width=device-width,user-scalable=no">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Asset - Real Estate</title>
      <link rel="stylesheet" href="{{ asset('css/web-css/bootstrap.min.css') }}">
      <link rel="stylesheet" href="{{ asset('css/fonts/stylesheet.css') }}">
      <link rel="stylesheet" href="{{ asset('css/web-css/style.css') }}">
      <link rel="stylesheet" href="{{ asset('css/web-css/mobile.css') }}">
      <style>
         .error {
          color: #a20707;
          font-size: 12px;
          margin: 0;
         }
      </style>
      @yield('stylesheet')
   </head>
   <body class="notLogged">
  @include('_partials.app-header')
  <main class="main-container bgGray">
  @yield('content')
  </main>
  
    <footer class="text-center pt20">
       <div class="container-footer">
          <div class="top-footer">
             <div class="footer-link">
                <ul>
                   <li><a href="contact-us.html">Contact Us</a></li>
                   <li><a href="about-us.html">About Us</a></li>
                   <li><a href="terms.html">Terms & Conditions</a></li>
                   <li><a href="privacy.html">Privacy Policy</a></li>
                   <li><a href="carrer.html">Careers</a></li>
                   <li><a href="faq.html">FAQ</a></li>
                   <li><a href="partner.html">Partnerships</a></li>
                </ul>
             </div>
              <ul class="socialLinks mb15 text-center">
                   <li><a href="javascript:void(0);" class="fbBg"><i class="fab fa-facebook-f"></i></a></li>
                   <li><a href="javascript:void(0);" class="twitterBg"><i class="fab fa-twitter"></i></a></li>
                   <li><a href="javascript:;" class="instagramBg"><i class="fab fa-instagram"></i></a></li>
                </ul>
          </div>
          <div class="bottom-fotter">
             <p>All Rights Reserved © AssetDotCo Inc</p>
          </div>
       </div>
    </footer>
      <script src="{{ asset('js/web-scripts/jquery-3.3.1.min.js')}}"></script>
      <script src="{{ asset('/js/jquery.validate.min.js') }}" ></script>
      <script src="{{ asset('js/web-scripts/bootstrap.min.js')}}"></script>
      <script type="text/javascript">
        $(".alert-success, .alert-info").fadeOut(3000);
      </script>
      @yield('scripts')
   </body>
</html>