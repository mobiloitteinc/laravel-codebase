@extends('layouts.app')

@section('stylesheet')
  <link rel="stylesheet" href="{{ asset('js/web-scripts/intl-tel-input-13.0.0/build/css/intlTelInput.css') }}"/>
  <style type="text/css">
    .iti-flag {background-image: url(" {{ asset('js/web-scripts/intl-tel-input-13.0.0/build/img/flags.png') }}");}

    @media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min--moz-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2 / 1), only screen and (min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {
      .iti-flag {background-image: url(" {{ asset('js/web-scripts/intl-tel-input-13.0.0/build/img/flags@2x.png') }}");}
    }
    .intl-tel-input {width: 100%;}
  </style>
@endsection

@section('content')
     <section class="common-section-main">
          <div class="common-container">
            <div class="row">
               <div class="col-12">
                  <div class="common-content-body">
                    <div class="form-main-heading">
                      <h3>SIGN UP</h3>
                    </div>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-body">
                      <form id="signup" method="POST" action="{{ route('register') }}">
                          @csrf
                        <p class="form-para">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </p>
                        <div class="row">
                          <div class="col-sm-6">
                            <div class=" form-group">
                              <input id="name" type="text" name="name" value="{{ old('name') }}" required autofocus class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }} custom-control" placeholder="Full Name*" minlength="3" maxlength="60">
                              @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class=" form-group">
                              <input id="email" type="email" name="email" value="{{ old('email') }}" required class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} custom-control" placeholder="Email*" maxlength="60">
                              @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class=" form-group">
                              <input type="tel" id="phone" class="form-control txtboxToFilter" name="phone_number" value="{{ old('phone_number') }}">
                              <input id="calling_code" type="hidden" name="country_code" value="">
                              @if ($errors->has('phone_number'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </span>
                              @elseif ($errors->has('country_code'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('country_code') }}</strong>
                                    </span>
                              @endif
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class=" form-group">
                              <input id="password" type="password" name="password" required class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} custom-control" placeholder="Password*" maxlength="16">
                              @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class=" form-group">
                              <input id="password_confirm" type="password" name="password_confirmation" required class="form-control custom-control" placeholder="Confirm Password*" maxlength="16">
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class=" form-group">
                              <div class="radio-block">
                                <div class="row">
                                    <label class="col-2">Gender</label>
                                      <div class="col-9 radioBox mobML15 {{ $errors->has('gender') ? ' is-invalid' : '' }}">
                                      <p class="radio-button">
                                        <input type="radio" id="male" value="Male" name="gender" checked="" required>
                                        <label for="male">Male</label>
                                      </p>
                                      <p class="radio-button">
                                        <input type="radio" id="female" value="Female" name="gender" required>
                                        <label for="female">Female</label>
                                      </p>
                                      @if ($errors->has('gender'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('gender') }}</strong>
                                        </span>
                                    @endif
                                    </div>
                                  </div>
                                </div>
                                
                            </div>
                          </div>
                          <!-- <div class="col-sm-12">
                            <div class=" form-group">
                              <input id="referral" type="text" name="referral" class="form-control custom-control" placeholder="Referral Code (Optional)" maxlength="16">
                              @if ($errors->has('referral'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('referral') }}</strong>
                                    </span>
                                @endif
                            </div>
                          </div> -->
                          <div class="col-sm-12">
                            <div class=" form-group">
                                <p class="custom-checkbox">
                                  <input type="checkbox" id="check1" required="" name="check" />
                                  <label for="check1" class="show_error">By signing up, you agree to <a href="#" data-toggle="modal" data-target="#master">Master agreement</a> and <a href="#">Privacy Policy.</a></label>
                                </p>
                            </div>
                          </div>
                        </div>

                        <div class="row">
                           <div class="col-6">
                              <button type="submit" class="btn btnYellow w-100 minHeight40 font16"><span>Next</button>
                           </div>
                           <div class="col-6">
                              <a href="{{ route('login') }}" class="btn btnGreen w-100 minHeight40 font16"><span>Cancel</span></a>
                           </div>
                        </div>

                      <p class="new-user"> Already User? <a href="{{ route('login') }}">Login</a>
                      </form>
                    </div>
                  </div>
              </div>
            </div>
          </div>
      </section>
      
      <!-- Master Aggrement Modal -->
      <div class="modal fade common-modal master-modal" id="master" role="dialog">
        <div class="modal-dialog">
        
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">MASTER AGREEMENT</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              
            </div>
            <div class="modal-body change-pass-body">
              <p class="modal-para">
                 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
              </p>
              <p class="modal-para">
                 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
              </p>
              <p class="modal-para">
                 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
              </p>
              <p class="modal-para">
                 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
              </p>
              <p class="modal-para">
                 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
              </p>
              <p class="modal-para">
                 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
              </p>


             
            </div>
           
          </div>
          
        </div>
      </div>
      <!--      -->
      @endsection

      @section('scripts')
      <script type="text/javascript" src="{{ asset('js/web-scripts/intl-tel-input-13.0.0/build/js/intlTelInput.js') }}"></script>
      <script type="text/javascript">
       
       function initCountryPhone( phone_input_id){
            // get the country data from the plugin

              var telInput = $('#'+phone_input_id);

              // init plugin
              telInput.intlTelInput({
                utilsScript: "{{ asset('js/web-scripts/intl-tel-input-13.0.0/build/js/utils.js') }}" // just for formatting/placeholders etc
                ,customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
                    return "Mobile number*";
                  }
              });
              
            }

       initCountryPhone("phone", "");


           $('#signup').validate({
              errorElement: 'span',
              focusInvalid: false,
              ignore: [],
              rules: {
                  email: {
                      required: true,
                       remote: {
                        url : "{{ route('valid-email')}}",
                        type : "GET",
                        dataFilter: function (data) {
                                var json = JSON.parse(data);
                                console.log(json)
                                if (json)
                                    return 'true';
                                else 
                                    return 'false';
                            },    
                        },
                  },
                  password: {
                      required: true,
                      minlength:8,
                      maxlength:16
                  },
                  phone_number:{
                      required:true,
                      digits:true,
                      minlength:6,
                      maxlength:20
                  },

                  password_confirmation: {
                      required:true,
                      equalTo: "#password"
                  },
                  name:{
                      required: true,
                  },
                  check:{
                    required:true
                  }
              },

              messages: {
                    email: {
                      required: 'Filling email address is mandatory.',
                      remote:  'Email address already exists.'
                    },
                    password: {
                      required: 'Filling password is mandatory.',
                   },
                    name:{
                      required: 'Filling full name is mandatory.'
                    },
                    password_confirmation:{
                      required:'Filling confirm password is mandatory.',
                      equalTo:'Password and confirm password should be same.'
                    },
                  phone_number:{
                      required:'Filling mobile number is mandatory.',
                      digits:"Please enter a valid mobile number."           
                  },
                  check:{
                      required:'Please accept the master agreement and privacy policy.',           
                  },
              },

              submitHandler: function(form) {
                $("#calling_code").val($("#phone").intlTelInput("getSelectedCountryData").dialCode);   
                form.submit();
              },
              errorPlacement: function(error, element) {
                    if (element.attr("name") == "check" ){
                      error.insertAfter(".custom-checkbox");
                    }

                    else
                        error.insertAfter(element);
                }
            });

           $(".txtboxToFilter").keydown(function (e) {
              // Allow: backspace, delete, tab, escape, enter and .
              if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                   // Allow: Ctrl+A, Command+A
                  (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
                   // Allow: home, end, left, right, down, up
                  (e.keyCode >= 35 && e.keyCode <= 40)) {
                       // let it happen, don't do anything
                       return;
              }
              // Ensure that it is a number and stop the keypress
              if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                  e.preventDefault();
              }
          });
      </script>

      @endsection