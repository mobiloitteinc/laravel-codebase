<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::group(['middleware' => ['website','address_verification']], function () {
    Route::get('/',function () {return view('welcome');});
});

Auth::routes();

// Social Login routes
Route::group(['middleware' => ['website','address_verification']], function () {
    Route::get('auth/{provider}', 'Auth\LoginController@redirectToProvider')->name('social.login');
    Route::get('auth/{provider}/callback', 'Auth\LoginController@handleProviderCallback');
    Route::get('/home',function () {return view('welcome');})->name('home');
});

//Forgot password
Route::group(['middleware' => ['website','address_verification']], function () {
    Route::post('/send-password-link', 'PasswordController@sendPasswordLink')->name('send-password-link');
    Route::get('reset-password/{token}', 'PasswordController@showPasswordResetForm');
    Route::post('reset-password/{token}', 'PasswordController@resetPassword');
    Route::get('/forgot-password',function () {return view('forgot_password');})->name('forgot-password');
});

//email address check
Route::group(['middleware' => ['website','address_verification']], function () {
    Route::get('valid-email','PasswordController@checkEmail')->name('valid-email');
});

///no redirection for address verification
Route::group(['middleware' => ['auth','website']], function () {
    Route::get('/confirm-address', 'Website\ProfileController@confirmAddress')->name('confirm-address');
    Route::get('/verification/type', 'Website\ProfileController@verificationType')->name('verification.type');
    Route::post('/confirm/address/submit', 'Website\ProfileController@confirmAddressSubmit')->name('address.confirm');
    Route::post('/verification/type/submit', 'Website\ProfileController@verificationTypeSubmit')->name('verification.submit');
    Route::post('/verification/type/otp', 'Website\ProfileController@verificationTypesubmission')->name('verification.otp');
    Route::get('otp-verification', 'Website\ProfileController@viewOtp')->name('otp-verification');
    Route::get('/document-verification', function(){return view('website.document_verification');})->name('document-verification');
    Route::get('/send/otp', 'Website\ProfileController@sendOTP')->name('send.otp');
    Route::get('/logout', 'HomeController@logout')->name('logout');
    Route::post('/check-for-verification', 'Website\ProfileController@checkVerificationType')->name('check-for-verification');
    $a = 'authenticated.';

    Route::get('/activate/{token}', ['as' => $a . 'activate', 'uses' => 'ActivateController@activate']);
    Route::get('/activate', ['as' => $a . 'activation-resend', 'uses' => 'ActivateController@resend']);

    Route::get('/mobile-number',function () {    return view('website.mobile_number'); })->name('mobile-number');
    Route::post('/mobile-number-social', 'Website\ProfileController@updateMobileNumber')->name('mobile-number-social');
});

//auth routes for website
Route::group(['middleware' => ['auth','website','address_verification']], function () {
    Route::get('/settings',function () {return view('settings');})->name('settings');
    //route profile controller
    Route::get('/profile','Website\ProfileController@loadProfile')->name('profile');
    Route::post('/save-edit-profile','Website\ProfileController@saveEditProfile')->name('save-edit-profile');
    Route::get('/edit-profile','Website\ProfileController@loadEditProfile')->name('edit-profile');
    Route::get('/validate-password', 'Website\ProfileController@validatePassword')->name('validate-password');
	Route::post('/update-password', 'Website\ProfileController@updatePassword')->name('update-password');
    //route for notice controller
    Route::get('/notice-board','Website\NoticeBoardController@load')->name('notice-board');
    //route for change password
    Route::get('/user-change-password','Website\ProfileController@loadChangePasswordTemplate')->name('user-change-password');
});

Route::group(['middleware' => ['website','address_verification']], function () {
    Route::get('not-activated', ['as' => 'not-activated', 'uses' => function () {return view('welcome');}]);
});


Route::group(['prefix' => 'user', 'middleware' => 'auth:user'], function(){
    $a = 'user.';
    Route::get('/', ['as' => $a . 'home', 'uses' => 'UserController@getHome']);

    Route::group(['middleware' => 'activated'], function ()
    {
        $m = 'activated.';
        Route::get('protected', ['as' => $m . 'protected', 'uses' => 'UserController@getProtected']);
    });
});


// Admin Routes
Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function () {
    Route::get('/',function () {return view('admin/login');})->name('admin.login');
    Route::post('index','Admin\UserController@index')->name('index'); 
});

Route::group(['prefix' => 'admin', 'middleware' => ['auth','admin']], function () {
    Route::get('/dashboard','Admin\UserController@dashboard')->name('admin.dashboard');  
    // User List
    Route::get('/user/list','Admin\UserController@userList')->name('admin.user.list');  
    // User view
    Route::get('/user/list/{id}','Admin\UserController@userView')->name('admin.user.view');  
    // Admin View
    Route::get('/user/view','Admin\UserController@adminView')->name('user.view');  
    //Edit admin Profile
    Route::get('/user/edit/profile','Admin\UserController@adminProfileEdit')->name('user.edit.profile');  
    //Update admin profile
    Route::post('/update/profile','Admin\UserController@adminUpdateProfile')->name('user.update.profile');  
    //Users Uploaded documents
    Route::get('/upload/user/list','Admin\UserController@documentUserList')->name('admin.upload.user.list'); 
    //Document view
    Route::get('/upload/user/list/{id}','Admin\UserController@documentView')->name('admin.upload.user.view');  
    // Document Approve
    Route::get('/upload/user/approve/{id}','Admin\UserController@documentApprove')->name('admin.upload.user.approve'); 
    // User Approved disapproved 
    Route::post('/user/active-deactive','Admin\UserController@userBlocked')->name('/user/active-deactive');
    // User Delete
    Route::post('/user/delete','Admin\UserController@userDelete')->name('/user/delete');
    // Property List
    Route::get('/property/list','Admin\PropertyController@PropertyList')->name('admin.property.list');  
    // Property Details
    Route::get('/property/list/{id}','Admin\PropertyController@PropertyView')->name('admin.property.view');  
    //logout functionality for admin
    Route::get('/logout', 'HomeController@adminLogout')->name('admin.logout');
});