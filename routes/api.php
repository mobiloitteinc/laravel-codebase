<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */


Route::group(['namespace' => 'API'],function () {
    // login Api
    Route::post('userlogin', 'ApiController@userlogin')->name('userlogin');
    // Signup Api
    Route::post('signup', 'ApiController@signup')->name('signup');
    // Tutorials Api
    Route::get('tutorials', 'ApiController@tutorials')->name('tutorials');
    // Socail Login
    Route::post('socialLogin', 'ApiController@socialLogin')->name('socialLogin');
    //Forgot Password Api
    Route::post('forgotPassword', 'ApiController@forgotPassword')->name('forgotPassword');
    //Static Content Api
    Route::post('staticContent', 'ApiController@staticContent')->name('staticContent');
});

Route::group(['middleware' => ['auth:api'],'namespace'=>'API'], function () {
    //Get Profile Api
    Route::post('getProfile', 'ApiController@getProfile')->name('getProfile');
    //Users Neighbourhood Api
    Route::post('usersNeighbourhood', 'ApiController@usersNeighbourhood')->name('usersNeighbourhood');
    //Change Password Api
    Route::post('changePassword', 'ApiController@changePassword')->name('changePassword');
    //Change Password Api
    Route::post('editProfile', 'ApiController@editProfile')->name('editProfile');
    //Notification On Off
    Route::post('notificationOnOff', 'ApiController@notificationOnOff')->name('notificationOnOff');
    //Follow Unfollow Api
    Route::post('followUser', 'ApiController@followUser')->name('followUser');
    //Logout Api
    Route::post('userlogout', 'ApiController@userlogout')->name('userlogout');
    //Rate App Api
    Route::post('rateApp', 'ApiController@rateApp')->name('rateApp');
    
    // Address Verification
    Route::post('addressVerfication', 'ApiController@addressVerfication')->name('addressVerfication');
    // OTP verification
    Route::post('otpVerfication', 'ApiController@otpVerfication')->name('otpVerfication');
    //Property Add
    Route::post('addProperty', 'PropertyController@addProperty')->name('addProperty');
    //Property Details
    Route::post('propertyDetails', 'PropertyController@propertyDetails')->name('propertyDetails');
    //Property List
    Route::post('propertyList', 'PropertyController@propertyList')->name('propertyList');
    //Attribute List
    Route::post('attributeList', 'PropertyController@attributeList')->name('attributeList');
    // Follow Property
    Route::post('followProperty', 'PropertyController@followProperty')->name('followProperty');
    //likeProperty
    Route::post('likeProperty', 'PropertyController@likeProperty')->name('likeProperty');
    // Comment Property
    Route::post('commentProperty', 'PropertyController@commentProperty')->name('commentProperty');
    //Comment List
    Route::post('commentList', 'PropertyController@commentList')->name('commentList');
    // Comment On Comment
    Route::post('commentOnComment', 'PropertyController@commentOnComment')->name('commentOnComment');
});