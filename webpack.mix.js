let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .styles(['resources/assets/css/admin-css/bootstrap.min.css',
   	'resources/assets/css/admin-css/font-awesome.min.css',
   	'resources/assets/css/admin-css/AdminLTE.min.css',
   	'resources/assets/css/admin-css/_all-skins.min.css',
   	'resources/assets/css/admin-css/blue.css',
	'resources/assets/css/admin-css/ionicons.min.css'], 'public/css/admin.css')
	.styles(['resources/assets/css/web-css/bootstrap.min.css',
   	'resources/assets/css/web-css/bootstrap-slider.css',
      'resources/assets/css/web-css/fontawesome-all.min.css',
   	'resources/assets/css/web-css/stylesheet',
   	'resources/assets/css/web-css/fonts.css',
   	'resources/assets/css/web-css/mobile.css',
	'resources/assets/css/web-css/style.css'], 'public/css/web.css')
   .scripts(['resources/assets/scripts/admin-script/jquery.min.js',
   	'resources/assets/scripts/admin-script/jquery-ui.min.js',
   	'resources/assets/scripts/admin-script/bootstrap.min.js',
   	'resources/assets/scripts/admin-script/adminlte.min.js',
   	'resources/assets/scripts/admin-script/dashboard.js',
	'resources/assets/scripts/admin-script/demo.js'], 'public/js/admin.js')
	.scripts(['resources/assets/scripts/web-script/jquery-3.3.1.min.js',
      'resources/assets/scripts/web-script/popper.min.js',
	'resources/assets/scripts/web-script/bootstrap.min.js',
      'resources/assets/scripts/web-script/bootstrap-slider.js',
   	'resources/assets/scripts/web-script/common.js',
   	'resources/assets/scripts/web-script/jquery.nicescroll.min.js'], 'public/js/web.js');
